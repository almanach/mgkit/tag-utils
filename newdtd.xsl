<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/XSL/Transform/1.0">
<xsl:template match="*"> 
begin 
  <xsl:for-each>
    <xsl:apply-templates select='.'/>
  </xsl:for-each>
end 
</xsl:template>
</xsl:stylesheet>
