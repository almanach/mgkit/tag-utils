#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data - 3;

my $dir = dirname($0);
my @output = split /\n/, `script/tag_converter -t lp $dir/sample.tag.xml | sed -e '/^%% Family/d'`;

foreach my $expected_output (@data) {
    my $output = shift @output;
    next if ($output =~ m/^%/);
    chomp $expected_output;
    is($output, $expected_output, "$expected_output");
}


__DATA__
%% Translated using tag_converter
%% Input files sample.tag.xml
%% Date Fri Feb 23 09:56:50 2007








tag_tree{	name => '0 det',
		family => '0 det',
		tree => tree id= det
			at <> det
	}.


tag_tree{	name => '1 prep_N2',
		family => '1 prep_N2',
		tree => tree id= 'PP'
			at pp(
				id= prep
				and bot= prep{pcas => _pcas_4}
				at <> prep,
				id= 'N2'
				at n2
			)
	}.


tag_tree{	name => '10 noun:agreement pnoun',
		family => '10 noun:agreement pnoun',
		tree => tree id= 'N2'
			and bot= n2{gender => _noun_gender_16, number => _noun_number_18}
			at n2(
				id= 'Np'
				and top= np{gender => _noun_gender_16, number => _noun_number_18}
				at <> np
			)
	}.


tag_tree{	name => '11 adj adj:agreement noun:shallow_auxiliary',
		family => '11 adj adj:agreement noun:shallow_auxiliary',
		tree => tree id= 'Nr'
			and bot= _noun_top_14 :: n{gender => _adj_gender_16, number => _adj_number_18}
			at n(
				id= adj
				and top= adj{gender => _adj_gender_16, number => _adj_number_18}
				at <> adj
			)
	}.


tag_tree{	name => '12 det:agreement n:agreement nc:agreement pnoun_as_cnoun',
		family => '12 det:agreement n:agreement nc:agreement pnoun_as_cnoun',
		tree => tree id= 'N2'
			and bot= n2{gender => _n_gender_16, number => _n_number_18}
			at n2(
				id= det
				and top= det{gender => _n_gender_16, number => _n_number_18}
				at det,
				id= 'N'
				and top= n{gender => _n_gender_16, number => _n_number_18}
				at n(
					id= 'Nc'
					at <> np
				)
			)
	}.


tag_tree{	name => '13 det:agreement n:agreement nc:agreement cnoun',
		family => '13 det:agreement n:agreement nc:agreement cnoun',
		tree => tree id= 'N2'
			and bot= n2{gender => _n_gender_16, number => _n_number_18}
			at n2(
				id= det
				and top= det{gender => _n_gender_16, number => _n_number_18}
				at det,
				id= 'N'
				and top= n{gender => _n_gender_16, number => _n_number_18}
				at n(
					id= 'Nc'
					at <> nc
				)
			)
	}.


tag_tree{	name => '2 prep_S',
		family => '2 prep_S',
		tree => tree id= 'PP'
			at pp(
				id= prep
				and bot= prep{pcas => _pcas_4}
				at <> prep,
				id= 'S'
				and top= s{mode => infinitive}
				at s
			)
	}.


tag_tree{	name => '3 prep_S',
		family => '3 prep_S',
		tree => tree id= 'N2'
			at 
	}.


tag_tree{	name => '4 csu',
		family => '4 csu',
		tree => tree id= 'WS'
			and bot= ws{lfg => _lfg_7}
			at ws(
				id= 'C'
				at <> csu,
				id= 'S'
				and top= s{lfg => _lfg_7}
				at s
			)
	}.


tag_tree{	name => '5 adv_leaf',
		family => '5 adv_leaf',
		tree => tree id= 'Adv'
			at <> adv
	}.


tag_tree{	name => '6 adv_s H:shallow_auxiliary',
		family => '6 adv_s H:shallow_auxiliary',
		tree => tree id= 'Sr'
			and bot= _H_top_14
			at s(
				id= 'Adv'
				at <> adv
			)
	}.


tag_tree{	name => '7 adv_adj H:shallow_auxiliary',
		family => '7 adv_adj H:shallow_auxiliary',
		tree => tree id= 'Adjr'
			and bot= _H_top_14
			at adj(
				id= 'Adv'
				at <> adv
			)
	}.


tag_tree{	name => '8 adv_adv H:shallow_auxiliary',
		family => '8 adv_adv H:shallow_auxiliary',
		tree => tree id= 'Advr'
			and bot= _H_top_14
			at adv(
				id= 'Adv'
				at <> adv
			)
	}.


tag_tree{	name => '9 adv_v H:shallow_auxiliary',
		family => '9 adv_v H:shallow_auxiliary',
		tree => tree id= 'Vr'
			and bot= _H_top_14
			at v(
				id= 'Adv'
				at <> adv
			)
	}.
