2008-12-12  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Node id can now be a variable X bound to a disjunction of values
	  (v1..vN), represented (in XML) by an attribute value of the form
	  "^ X
	  ^ v1 .. vN", and (in DyALog) by X::nodeid[v1,..,vN] where nodeid
	  is
	  given by $map{lp}{nodeid}, by default 'nodeid'

2008-12-05  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Node id can now be a disjunction of values, represented (in XML)
	  by a
	  space-separated list, and (in DyALog) by a finite set built upon
	  $map{lp}{nodeid}, by default 'nodeid'

2008-09-17  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Fixed to avoid some stupid warning on main::STDOUT.

	* Added instruction order_fs in map to force some feature ordering
	  in feature structures (but does not seem to gain in FRMG)

2008-09-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Simpler handling of disjunctive lexical values using finite_set
	  lex[...]

2008-09-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Added handling of multi-values for lexical nodes.

2008-08-28 1.12
	- Change from ATOLL to ALPAGE

2007...
	- Added tests
	- Moved files to lib/ and script/
	- Doc
	- Added README, INSTALL, LICENCE
	- Version number is now only specified in TAG.pm

2006-12-11 1.11

2006-05-02 1.10
	- Try to setup a correct Makefile.PL wrt to yapp and
	  TAG/LP/Parser.pm that is generated with yapp.
	- Lexical node with no specified lex value are now allowed. At
	  parsing time, any word may fill the node.
	- Lexical nodes may have an id
	- Updated administration of tag_utils
	- Check on coanchor
	- Quick fix to handle deep FS in equations
	- Added DBI modules to access Database
	- Added tag.sql (Database Schema for TAG)
	- Replaced tag2xml.pl and xml2tag by tag_converter
	- Updated DTD and corresponding .pm
	- Added TAG::XML::Writer
	- Fixed pb with ut8<->latin-1 in LP
	- Added old version of LP->XML (TAG::LP::Parser.yp and tag2xml.pl).
	
2000-07-26 0.01
	- original version; created by h2xs 1.20 with options
		-n TAG -X

