#  use XML::XSLT;

#  my $xsl = shift;
#  my $xmlfile = shift;

#  my $xslt = XML::XSLT->new ($xsl, warnings => 1);

#  print $xslt->serve($xmlfile);

#  $xslt->dispose();


#!/usr/bin/perl

use strict;
use warnings;
use XML::Twig;

my $file = shift;

my $twig = new XML::Twig( TwigHandlers => { 'lemma' => \&convert,
					    'coanchor' => \&convert2,
					    'equation' => \&convert3,
					    'node[@type="anchor"]' => \&convert4,
					    },
			  PrettyPrint => 'indented' ,
#			  DiscardSpacesIn => ['lemmaref'],
#                          KeepEncoding => 1,
                          );

$twig->parsefile($file);
$twig->print;

sub convert {
    my ($twig, $elt)= @_;
    my $name = $elt->att('name');
    my $cat = $elt->att('cat');
    foreach my $anchor ($elt->children) {
	$anchor->set_gi('lexicalization');
	my $coanchor = XML::Twig::Elt->new( 'anchor', 
					    { node => $elt->att('cat').'_' },
					  XML::Twig::Elt->new('lemmaref', 
							      { 'cat' => $elt->att('cat'), 
								'name' => $elt->att('name')},
							      '#EMPTY'));
	$coanchor->paste($anchor);
	my @families = ($anchor->att('tree_id') =~ /\@name=(\w+)/g);
	foreach my $family (@families) {
	  XML::Twig::Elt->new('familyref',{'name'=>$family},'#EMPTY')->paste($anchor);
	}
	$anchor->del_atts;
    }
    $elt->erase;
}

sub convert2 {
    my ($twig, $elt)= @_;
    $elt->set_gi('anchor');
    my $node = $elt->att('node_id');
    $elt->del_att('node_id');
    $elt->set_att('node',$node);
}

sub convert3 {
    my ($twig, $elt)= @_;
    my $node = $elt->att('node_id');
    $elt->del_att('node_id');
    $elt->set_att('node',$node);
}

sub convert4 {
    my ($twig, $elt)= @_;
    if (!$elt->att('id')) {
	my $cat = $elt->att('cat');
	$elt->set_att('id',$cat.'_');
    }
}
