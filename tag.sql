/*
	Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

	Database Schema for Tree Adjoing Grammars [TAG]

	$Id$

*/

-- Family table
create table families (
	name	text,
	tree	text
);

-- Tree table
create table trees (
	name	text,
	type	text,
	root	int
);

-- Node tables
create table nodes (
	ref	int,
	tree	text,
	parent  int,		-- -1 if no parent (root)
	index	int,		-- -1 if no parent (root)
	id	text,
	cat	text,
	lex	text,
	type	text,		-- in {std,subst,lex,foot,anchor}
	adj	text,		-- in {yes,no,strict}
	top	int,		-- fs ref or -1
	bot	int		-- fs ref or -1
);

-- Lexicon table
create table morph (
	lex		text
);

-- LemmaRef table
create table lemmaref (
	lex	text,
	name	text,
	cat	text,
	feature int		-- fs ref
);

-- Lemma tables

create table lemma (
	ref		int,
	name		text,
	cat		text
);

-- Anchor tables

create table anchors (
	ref		int,
	lemma		int -- lemma ref
);

create table anchors_family (
	ref		int,
	family		text
);

-- Coanchor tables

create table coanchors (
	ref		int,
	anchor		int,	-- anchor ref
	node_id		text
);

create table coanchors_lex (
	ref		int,
	lex		text
);

-- Equation table
create table equations (
	anchor		int,	-- anchor ref
	node_id		text,
	type		text,	-- in {top,bot}
	fs		int
);

-- Feature Structure table
create table fs (
	ref		int,
	name		text,
	type		text,	-- in {val,link,plus,minus}
	val		text
);






