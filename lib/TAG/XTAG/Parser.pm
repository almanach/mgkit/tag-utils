package TAG::XTAG::Parser;

use strict;
use warnings;   
use Text::Balanced;
#use Getopt::Mixed;                                                 
use TAG;       

#our @ISA = qw{TAG::XTAG::Parser};      
#our ($opt_use_map, $opt_grammar_name, $opt_tree_file, $opt_family_file);

#Getopt::Mixed::getOptions(
#		     "use_map=s",
#		     "grammar_name=s",
#		     "tree_file=s"
#                     );



my %bottom_features=();
my %top_features=();
my %bind=();
my %node_names=();

my $rnd_id = 0;
    
our $map;


sub add_feature{
    my $feat = shift;
    my $value = shift;
    if ($feat =~ m/\s*(\w*)\.b:<([^\>]*)>/){
	$bottom_features{$1} = [$2,$value,@{$bottom_features{$1}}];
    }elsif ($feat =~ m/\s*(\w*)(.t)?:<([^\>]*)>/){
	$top_features{$1} = [$3,$value,@{$top_features{$1}}];
    }else{
	die "probleme de feature \n";
    }
}

sub deref{
    my $name = shift;
    while ($bind{$name}){
	$name = $bind{$name};
    }
    return $name;
}

sub bind_var{
#    print "--- -- bind_var\n";
    my $f1 = shift;
    my $f2 = shift;
    $f1 = deref($f1);
    $f2 = deref($f2);
    unless ($f1 eq $f2){
	$bind{$f1}=$f2;
    }
}

sub bind_val{
    my $feat = shift;
    my $value = shift;
# may be verify if the feature is not already bound
    if ($feat =~ m/\s*(\w*)\.b:<([^\>]*)>/){
#	print "----$1--$2\n";
	if ($bottom_features{$1}){
	    $bottom_features{$1} = [$2,$value,@{$bottom_features{$1}}];
	}else{
	    $bottom_features{$1} = [$2,$value];
	}
    }elsif ($feat =~ m/\s*(\w*)(.t)?:<([^\>]*)>/){
#	print "----$1--$3\n";
	if ($top_features{$1}){
	    $top_features{$1} = [$3,$value,@{$top_features{$1}}];
	}else{
	    $top_features{$1} = [$3,$value];
	}	    
    }else{
	die "probleme de feature \n";
    }
}

    

sub tab_features{
    my $ch = shift;
    $ch =~ s/\"//og;
    my @feat = split /\n/, $ch;
    my $var_index = 0;
    %top_features = ();
    %bottom_features = ();
    %bind = ();
    foreach (@feat){
#	print " *** $_\n";
	s/^\s+//;
	s/\s+$//;
	my @fst = split(/\s*=\s*/,$_);
	if ($fst[1]){
	    if ($fst[1] =~ /(\S*):\<(.*)\>/){
		bind_var($fst[0],$fst[1]);
	    }elsif($fst[1])  {
		bind_val($fst[0],$fst[1]);
	    }
	}
    }
#    print "/";
    my $key;
    my @tank = (keys %bind);
    while (@tank){
	$key = shift @tank;
	my $end = deref ($key);
#	print ",,$key,$end,\n";
	if ($end =~ m/^X(\d+)$/){
#	    print "|||\n";
	    bind_val($key,$end);
	}else{
#	    push(@tank,$end);
	    bind_var($end,"X$var_index");
	    bind_val($end,"X$var_index");
	    bind_val($key,"X$var_index");
	    $var_index++;
	}
    }   
#    print %topfeatures;
#    print "//\n";
}

sub explose{
    my $ch = shift;
    pos $ch = 1;
    (my $tree_name, my $remainder) = Text::Balanced::extract_delimited($ch,'"');
    $tree_name =~ s/\002/init-/;
    $tree_name =~ s/\003/aux-/;
    $tree_name =~ s/\"//og;
#    print "---\n$ch\n--\n";
    $ch =~  m/:COMMENTS/ig;
    (my $comment, $remainder) = Text::Balanced::extract_delimited($ch,'"'); 
    pos $ch = 1;
    $ch =~  m/:UNIFICATION-EQUATIONS/ig;
    (my $equations, $remainder) = Text::Balanced::extract_delimited($ch,'"'); 
#    print "==== $comm\n";
#    print "=== $tree_name\n";
    tab_features($equations);
#    foreach (keys %top_features){
#	print "*** $_ @{$top_features{$_}} \n";
#    }
#    foreach (keys %bottom_features){
#	print "/// $_ @{$bottom_features{$_}} \n";
#    }
#    foreach (keys %bind){
#	print "???$_??$bind{$_}?? \n";
#    }
    # returns also features, using side-effects
#    print "^^^^$comment\n";
    return ($tree_name,$comment);
}

sub feat_val{
    my $name = shift;
    my $val = shift;
    if ($val =~ m/^X(\d+)$/){
	return TAG::Comp->new(name=>$name, id=>$val);
    }else{
	my @values;
	foreach(split('/',$val)){
	    if ($val =~ m/^\s*-\s*$/){
		push(@values,TAG::Minus->new());
	    }elsif($val =~ m/^\s*\+\s*$/){
		push(@values,TAG::Plus->new());
	    }else{
		push(@values,TAG::Val->new(text=>$_));
	    }
	}
	return TAG::Comp->new(name=>$name, 
			      value=>[@values]);
    }
}

sub get_features{
    my $list = shift;
    my $id = shift;
    my $type = shift;
    my $fnam;
    my $feat;
    my %res=();
    if ($list){
#	print "+++ @$list\n";
	while (@$list){
	    $fnam = shift @$list;
	    $res{$fnam}=feat_val($fnam,shift @$list);
	}
	$feat=TAG::Feature->new({f=>\%res});
	return $feat; 
#return TAG::Equation->new(node_id=>$id, type=>$type, feature=> $feat);
    }
}


sub build_tree_node{
    my ($cat, $id, $headp, $adjoip, $footp, $substp, $tname) = @_;
    my $node = TAG::Node->new();
#    my $nid = "$cat\_$id\_$rnd_id";
    my $nid = "$cat";
    if ($id  ne ""){
	$nid = "$cat\_$id";
    }
    if ($node_names{$nid}){
	if (($top_features{$nid}) || ($bottom_features{$nid})){
	    print STDERR "Warning: ambiguous node name with features $nid in tree $tname\n";
	    $top_features{"$nid\_\_$rnd_id"}=$top_features{$nid};
	    $bottom_features{"$nid\_\_$rnd_id"}=$bottom_features{$nid};
	    $nid="$nid\_\_$rnd_id";
	    $rnd_id++;
	    
	}else{
#	    print STDERR "Simple trace: ambiguous name for node $nid\n";
	    $nid="$nid\_\_$rnd_id";
	    $rnd_id++;
	}
    }
    $node_names{$nid}=1;
#    if ($nid =~ s/_$/_$rnd_id/){
#	$rnd_id++;
#    }
    unless($$map{$cat} eq "lexical"){
	$node->{cat} = $cat;
	$node->{id} = $nid;
    }
#    print "---$cat $id, $headp, $adjoip, $footp, $substp\n";
#    print join('|',keys %$map);
#    print "---\n";
    if ($$map{$cat} eq "lexical"){
	$node->{type}='lex';
	$node->{adj}='no'; # je doute un peu, la
	if ($cat =~ /^_F/){
	    $node->{lex}="";
	}else{	
	    $node->{lex}=$cat;
	}
	if ($id){
	    $node->{id}=$nid;
	}
    }elsif($footp){
	$node->{type}='foot';
	$node->{adj}='no';
    }elsif($headp){
	$node->{type}='anchor';
	$node->{adj}='yes';
    }elsif($substp){
	$node->{type}='subst';
	$node->{adj}='no';
    }else{
	$node->{type}='std';
	$node->{adj}='yes';
    }
    if($adjoip){
	$node->{adj}=$adjoip;
    }
    $node->{top}=get_features($top_features{$nid},$nid,'top');
    $node->{bot}=get_features($bottom_features{$nid},$nid,'bot');
    return $node;
}

sub noeud{
    my $ch = shift;
    my $tname = shift;
    my $cat;
    my $id;
    my $headp;
    my $adjoip;
    my $footp;
    my $substp;
    if ($ch =~ m/\"([\w\006\`\']*)\"\s*\.\s*\"(\w*)\"/){
	$cat=$1;
	$id=$2;
	$cat =~ s/\006/_F/; 
#	$cat =~ s/\'//; 
# 	$cat =~ s/\`//; 
   }else{
	die "ill formed node: $ch\n";
    }
    if ($ch =~ m/:substp\s*t\W/i){
	$substp = 1;
    }else{
	$substp =0;
    }
    if ($ch =~ m/:headp\s*t\W/i){
	$headp = 1;
    }else{
	$headp =0;
    }
    if ($ch =~ m/:footp\s*t\W/i){
	$footp = 1;
    }else{
	$footp =0;
    }
    if ($ch =~ m/:na/i){
	$adjoip = "no";
    }
#    print "($cat, $id, head=$headp, adjoi=$adjoip, foot=$footp, subst=$substp)\n";

    return build_tree_node($cat, $id, $headp, $adjoip, $footp, $substp, $tname);
}  

sub tree{
    my $ch = shift;
    my $tname = shift;
    my $root;
    my $daughter;
    my $trash;
    my $node;
    my @child;
    chop $ch;
    pos $ch=1;
    ($root,$trash) = Text::Balanced::extract_bracketed($ch, '()');
    $node=noeud($root,$tname);
#    noeud $root;
    while (1){
	($daughter,$trash) = Text::Balanced::extract_bracketed($ch, '()');
	unless ($daughter){last;}
	push (@child,tree($daughter,$tname));
    }
    $node->{child} = [@child];
    return $node;
}

sub read_multiline_parent{
    my $file =shift;
    my $text = shift;
    my $exact;
    my $fin;
    my @truc;
    my $prefix;
    my $cpt = 0;
    while (<$file>){
#	@truc = split /;/,$_;
	@truc = ($_);   # modif ad hoc le 26 juin 2003 pour NBENG
	next if ($truc[0] =~ /^\s*$/);
	$text=$text . $truc[0] . ' ';
	while ($truc[0] =~ m/\(/g){
	    $cpt++;
	}
	pos $truc[0] = 0;
	while ($truc[0] =~ m/\)/g){
	    $cpt--;
	}
	if ($cpt == 0){
	    last;
	}
    }
    ($exact, $fin,$prefix) = Text::Balanced::extract_bracketed($text,'()');
    return ($exact, $fin);
}

sub parse_tree_file_aux{
    my $tag_document = shift;
    my $fname = shift;
    my $file_prefix = shift;
    my $family = shift;
    my $text="";
    my $exact;
    my $tname;
    my $comment;
    my $tree;
    my $root;
    my $type;
    my $fentry;
    my $desc;
    my $family_name=$file_prefix;
#    print "--- $fname|$family|\n";
    open LIRE, $fname;
    while (! eof(LIRE)){
	($exact,$text)= read_multiline_parent(\*LIRE,$text);
	($tname,$comment) = explose($exact);
	($exact,$text)= read_multiline_parent(\*LIRE,$text);
	$rnd_id = 0;
	%node_names=();
#	print STDERR "$tname\n";
	$root = tree($exact,$tname);
	if ($tname =~ /^init/){
	    $type = 'initial';
	}else{
	    $type = 'auxiliary';
	}
#	print "****$comment\n";
	$desc = TAG::Desc->new(text=>$comment);
	$tree = TAG::Tree->new(name=>$tname, family=>$family, 
			   type=>$type, root=>$root, desc=>[$desc]);
	$tag_document->{trees}{$tname}=$tree;
	unless ($family){
	    $family_name=$tname;
	}
	$fentry =  $tag_document->{families}{$family_name};
	if (!$fentry) {
	    $fentry = TAG::Family->new({name=>$family_name,trees=>[]});
#	    print "++++++$family\n";
	    $tag_document->{families}{$family_name}=$fentry;
#	    print "++++++----------";
	}
	push(@{$fentry->{'trees'}},$tree->{name});
    }
    close LIRE;
}

 
sub new{
    my $class=shift;
    $map = shift;
    return bless {};
}


sub TAG::Document::xtag_parse{
    my $self = shift;
    my $file = shift;
    my $force = shift;
    my $is_family = shift;
    my $family_name;
    my $file_prefix = $file;
#    print "** $is_family\n";
    unless ($self->{families}){
	$self->{families}={};
    }
    unless ($self->{desc}){
	$self->{desc}=[];
    }
    $file_prefix=~s/\.$$map{family_file_extension}//;
    $file_prefix=~s/\.$$map{tree_file_extension}//;
    if ($file){
	unless ($force){
	    if ($$map{$file_prefix} eq "tree_file"){
		$family_name = 0;
	    }elsif($$map{$file_prefix} eq "family_file"){
		$family_name = $file_prefix;
	    }else{
		die "Unregistered grammar file: $file\n";
	    }
	}
	parse_tree_file_aux($self,$file,$file_prefix,$family_name);
    }else{
	my @tree_files = grep({$$map{$_} eq "tree_file"} keys %$map);
	foreach(@tree_files){
	    parse_tree_file_aux($self,$file,$file_prefix,0);
	}
	my @family_files = grep({$$map{$_} eq "family_file"} keys %$map);
	foreach(@family_files){
	    $file =~ m/^(.+)\.$$map{family_file_extension}$/;
	    $family_name = $1;
	    parse_tree_file_aux($self,$file,$file_prefix,1);
	}
    }
    return $self;
}
    

sub morph_entry{
    my $self = shift;
    my $str = shift;
    my ($form,@lemref,$lem,$cat,@temp_list,@res);
    $str =~ /^(\S+)\s+/;
    $form = $1;
    $str =~ s/^$form\s+//;
    my @lem_list = split(/\#/,$str);
    foreach (@lem_list){
	/(\S+)\s+(\S+)(\s+(.*))?$/;
	$lem = $1;
	$cat = $2;
	@temp_list = split(/\s+/,$4);
	push(@res,
	   TAG::LemmaRef->new({name => $lem, cat => $cat, 
				     templates => [map {"\@$_"}  @temp_list]})
	     );
    }
    $self->{lexicon}{$form}=
      TAG::Lexicon->new({ lex => $form, lemma => [@res]});
}


sub TAG::Document::xtagmorph_parse{
    my $self = shift;
    my $file = shift;
    open LIRE, $file;
    while (<LIRE>){
	morph_entry($self,$_);
    }
}

sub lex_entry{
    my $self = shift;
    my $str = shift;
    my @pos;
    my @families;
    my @trees;
    my @temp;
    $str =~ s/<<INDEX>>[^\<]+<<EN/<<EN/;
    while ($str =~ /^<<ENTRY>>([^\<]+)<<POS>>([^\<]+)(<<.*)/){
	push(@pos,$1);
	push(@pos,$2);
	$str = $3;
    }
    if ($str =~ /^\<\<FAMILY\>\>([^\<]+)(\<.*)?/){
	@families = split(/\s+/,$1);
	$str = $2;
    }elsif($str =~ /^<<TREES>>([^\<]+)(<.*)?/){
	@trees = split(/\s+/,$1);
	$str = $2;
    }else{
	die "van den lamme";
    }
    if ($str =~/^<<FEATURES>>(.*)$/){
	@temp = split(/\s+/,$1);
    }
    # je me suis arrete la
    print "_____________\n POS: ";
    foreach (@pos){
	print "$_ ";
    }
    print "\n Str: ";
    foreach (@trees){
	print "$_ ";
    }
    foreach (@families){
	print "$_ ";
    }
    print "\n TEMP: ";
    foreach (@temp){
	print "$_ ";
    }
    print "\n";
}

sub TAG::Document::xtaglex_parse{
    my $self = shift;
    my $file = shift;
    open LIRE, $file;
    while (<LIRE>){
	lex_entry($self,$_);
    }
}


1;

__END__

=head1 NAME

TAG::XTAG::Parser

=head1 SYNOPSIS

 use TAG::XTAG::Parser;
 
=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut	    






