# $Id$
package TAG::RCG;

require 5.005;
use strict;
use warnings;
use TAG;

#our $treenb=1;
our $nodes=0;
our %info=();
our $trees=1;

sub TAG::Document::RCG {
    my $self = shift;
    $info{'doc'}=$self;
    $info{'adj'}={};
    my @rules = ();
    push(@rules, <<EOF);
%% Rcg $TAG::RCG::trees: Axiom $self->{'axiom'}
axiom(X) --> $self->{'axiom'}1(X).
EOF
    $TAG::RCG::trees++;
    push(@rules,
	 map($self->{'families'}{$_}->RCG($self),
	     sort (keys %{$self->{'families'}})));
    foreach my $cat (sort keys %{$info{'adj'}}){
	push(@rules,<<EOF);
%% Rcg $TAG::RCG::trees: void adj $cat
${cat}2( , ) --> .
EOF
    $TAG::RCG::trees++
    }
    return @rules;
}

sub TAG::Document::RCGlex {
    my $self = shift;
    $info{'doc'}=$self;
    return  map($self->{'lexicon'}{$_}->RCGlex,
		sort (keys %{$self->{'lexicon'}}));
}

sub TAG::Family::RCG {
    my $self = shift;
    my $doc = shift;
    my $family = $self->{'name'};
    return map($doc->{'trees'}{$_}->RCG($family),
	       sort @{$self->{'trees'}});
}

sub TAG::Tree::RCG {
    my $self = shift;
    my $family = shift;
    my $root= $self->{'root'};
    my $label = $root->{'cat'} . ($self->{'type'} eq 'initial' ? 1 : 2);
    my ($head,$body) = $root->RCG($family);
    my $treenb = $TAG::RCG::trees++;
    $TAG::RCG::nodes=0;
    $info{'adj'}{$root->{'cat'}} = 1 if ($self->{'type'} eq 'initial');
    return <<EOF ;
%% Rcg $treenb: Tree $self->{'name'}
$label($head) -->
\t$body.
EOF
}

sub TAG::Node::RCG {
    my $self = shift;
    my $family = shift;
    my $type = $self->{'type'};
    my $id = $self->{'id'};
    if ($type eq 'foot') {
	$id = $TAG::RCG::nodes++ unless ($id);
	return ( ',' , '' ) 
    }
    if ($type eq 'lex') {
	my $lex = $self->{'lex'};
	$lex = "\"$lex\"" if ($lex);
	return (  $lex , '' ) ;
    }
    if ($type eq 'subst') {
	$id = $TAG::RCG::nodes++ unless ($id);
	my $var = "S$id";
	return ( $var, "$self->{'cat'}1($var)" );
    }
    my @head = ();
    my @body = ();
    if ($type eq 'anchor') {
	push(@head,"\"+${family}_$self->{'cat'}\"");
    } else {
	foreach my $child (@{$self->{'child'}}) {
	    my ($chead,$cbody) = $child->RCG($family);
	    push(@head,$chead) if ($chead);
	    push(@body,$cbody) if ($cbody);
	}
    }
    $id = $TAG::RCG::nodes++ unless ($id);
    if ( $self->{'adj'} eq 'yes' ){
	my $varL = "L$id";
	my $varR = "R$id";
	$info{'adj'}{$self->{'cat'}} = 0 unless (defined $info{'adj'}{$self->{'cat'}});
	unshift @body, "$self->{'cat'}2($varL,$varR)";
	@head = ($varL,@head,$varR);
    }
    return (join(" ",@head), join("\n\t",@body));
}

sub TAG::Lexicon::RCGlex {
    my $self = shift;
    my @families = map($_->RCGlex,@{$self->{'lemma'}});
    return "$self->{lex} { @families }";
}

sub TAG::LemmaRef::RCGlex {
    my $self = shift;
    my $name = $self->{'name'};
    my $cat = $self->{'cat'};
    my $lemma = $info{'doc'}{'lemma'}{$name}{$cat};
    return map($_->RCGlex($self->{'cat'}),@{$lemma->{'anchors'}}) if ($lemma);
}

sub TAG::Anchor::RCGlex {
    my $self = shift;
    my $cat = shift;
    return map("+${_}_$cat",@{$self->{'family'}});
}

1;

__END__

=head1 NAME

TAG::RCG - output content of a Tree Adjoining Grammar (TAG) as a Range Concatenation Grammar (RCG)

=head1 SYNOPSIS

 use TAG::RCG;
 
 # using RCG method on a TAG::Document 
 @rules = $tag_document->RCG();

=head1 DESCRIPTION

Calling `C<RCG>' on an TAG object prints it as a Range Concatenation
Grammar. Some information are lost during the process.

=head1 OPTIONS

    none.

=over 4

=back

=head1 EXAMPLES

Here is an example of parsing a TAG in XML format and emitting it as a RCG:

    use TAG::XML::Parser;
    use TAG::RCG;

    my $parser = TAG::XML::Parser->new ();
    my $doc = $parser->parsefile( $grammarfile );

    print join("\n",$doc->RCG()),"\n"; 

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
