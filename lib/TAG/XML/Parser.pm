# $Id$
package TAG::XML::Parser;

require 5.005;
use strict;
use warnings;
use TAG;
use XML::Parser;
use UNIVERSAL;
use Scalar::Util qw{blessed};

our @ISA = qw{XML::Parser};

our $doc;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(Pkg=>__PACKAGE__,
				  Style=>'Subs',
				  @_);
    $self->setHandlers(Final => \&Final,
		       Init => \&Init,
		       Char => \&Char
		       );
    return $self;
}

sub Char {
  my $expat = shift;
  my $text = shift;
  $text =~ s/^\s+//o;
  $text =~ s/\s+$//o;
  $expat->{Current}{'text'}=$text unless ($text  =~ /^\s*$/);
}

sub Final {
  my $expat = shift;
  delete $expat->{Current};
  delete $expat->{CurrentTree};
  delete $expat->{Where};
  delete $expat->{Stack};
  return $expat->{Document};
}

sub Init {
    my $expat = shift;
    $expat->{Document} = $doc || TAG::Document->new( 'axiom' => 's' );
}

sub tag {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    $expat->{Document}{'axiom'} = $attr{'axiom'} if (defined $attr{'axiom'});
    $expat->push($expat->{Document});
}

sub tag_ {
    shift->pop();
}

sub morph {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $lex = $attr{'lex'};
    my $lexicon;
    if (defined $expat->{Document}{'lexicon'}{$lex}) {
      $lexicon = $expat->{Document}{'lexicon'}{$lex};
    } else {
      $expat->{Document}{'lexicon'}{$lex} 
	= $lexicon 
	  = TAG::Lexicon->new( {lex => $lex, lemma => []});
    }
    $expat->push($lexicon);
  }

sub morph_ {
    shift->pop();
}

sub lemmaref {
    my $expat = shift;
    shift;
    my $lemmaref = TAG::LemmaRef->new( { @_ } );
    push( @{ $expat->{Current}{'lemma'} }, $lemmaref );
    $expat->push($lemmaref);
}

sub lemmaref_ {
    shift->pop();
}

sub fs {
    my $expat = shift;
    my $tag = shift;
    my $feature = TAG::Feature->new( { @_ } );
    $feature->{Save_Where} = $expat->push_val($feature,'feature');
    $expat->push($feature);
    $expat->{Where} = 'value';
}

sub fs_ {
  my $expat = shift;
  $expat->{Where} = $expat->{Current}{Save_Where};
  $expat->pop();
}

sub f {
    my $expat = shift;
    my $tag = shift;
    my $comp = TAG::Comp->new( { @_, value =>[] } );
    $expat->{Current}{'f'}{$comp->{'name'}} = $comp;
    $expat->push($comp);
}

sub f_ {
    shift->pop;
}

######################################################################
# <val> is now obsolete !  Sept 2003 EVDLC
# use <sym value=""/> instead

sub val {
    my $expat = shift;
    my $tag = shift;
    my $val = TAG::Val->new();
    push( @{ $expat->{Current}{'value'} },$val);
    $expat->push($val);
}

sub val_ {
    shift->pop;
}

######################################################################
# Added element sym to replace val

sub sym {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $val = TAG::Val->new(text=>$attr{value});
    push( @{ $expat->{Current}{'value'} },$val);
}

## Moved to symbol !

sub symbol {
  my $expat = shift;
  my $tag = shift;
  my %attr = @_;
  $expat->push_val(TAG::Val->new(text=>$attr{value}));
}

######################################################################
# Added vAlt for disjunction of values

sub vAlt {
  my $expat = shift;
  my $tag = shift;
  my $alt = TAG::VAlt->new({@_,value => []});
  $alt->{Save_Where} = $expat->push_val($alt);
  $expat->push($alt);
  $expat->{Where} = 'value';
}

sub vAlt_ {
  my $expat = shift;
  $expat->{Where} = $expat->{Current}{Save_Where};
  $expat->pop();
}

######################################################################

sub var {
    my $expat = shift;
    my $tag = shift;
    my $var = TAG::Var->new({@_,value => []});
    $var->{Save_Where} = $expat->push_val($var);
    $expat->push($var);
    $expat->{Where} = 'value';
  }

sub var_ {
  my $expat = shift;
  $expat->{Where} = $expat->{Current}{Save_Where};
  $expat->pop();
}

######################################################################

sub not {
  my $expat = shift;
  my $tag = shift;
  my $not = TAG::Not->new({@_,value => []});
  $not->{Save_Where} = $expat->push_val($not);
  $expat->push($not);
  $expat->{Where} = 'value';
}

sub not_ {
  my $expat = shift;
  $expat->{Where} = $expat->{Current}{Save_Where};
  $expat->pop();
}

######################################################################

sub reducer {
  my $expat = shift;
  shift;
  my $reducer = TAG::Reducer->new({@_,'value' => [] });
  push(@{$expat->{Current}{'reducers'}},$reducer);
  $expat->push($reducer);
  $expat->{Where} = 'value';
}

sub reducer_ {
  my $expat = shift;
  $expat->pop();
  undef $expat->{Where};
}

######################################################################

sub link {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    $expat->{Current}{'id'}=$attr{'id'};
}

sub lemma {
    my $expat = shift;
    my $tag = shift;
    my $lemma = TAG::Lemma->new( { @_ });
    $expat->{Document}{'lemma'}{$lemma->{'name'}}{$lemma->{'cat'}}=$lemma;
    $expat->push($lemma);
}

sub lemma_ {
    shift->pop();
}

sub anchor {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $anchor = TAG::Anchor->new();
    my @families = ($attr{'tree_id'} =~ /\@family=([^\[\], ]+)/og);         ## deprecated
    @families = ($attr{'tree_id'} =~ /\@name=([^\[\], ]+)/og) unless (@families);
    @families = split(/\s+/,$attr{'tree_id'}) unless (@families);           ## deprecated
    $anchor->{'family'} = [  @families ];
    push @{ $expat->{Current}{'anchors'} }, $anchor;
    $expat->push($anchor);
}

sub anchor_ {
    shift->pop();
}

sub equation {
    my $expat = shift;
    shift;
    my $equation = TAG::Equation->new( { @_ } );
    push @{ $expat->{Current}{'equations'} }, $equation;
    $expat->push($equation);
}

sub equation_ {
    shift->pop();
}

sub coanchor {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $coanchor = TAG::Coanchor->new();
    $coanchor->{'node_id'} = $attr{'name'} || $attr{'node_id'}; ## attr 'name' is deprecated
    push @{ $expat->{Current}{'coanchors'} }, $coanchor;
    $expat->push($coanchor);
}

sub coanchor_ {
    shift->pop();
}

sub lex {
    my $expat = shift;
    my $lex = TAG::Lex->new();
    push @{ $expat->{Current}{'lex'} }, $lex;
    $expat->push($lex);
}

sub lex_ {
    shift->pop();
}

sub plus {
  my $expat = shift;
  $expat->push_val(TAG::Plus->new());
}

sub minus {
  my $expat = shift;
  $expat->push_val(TAG::Minus->new);
}

sub family {
    my $expat = shift;
    shift;
    my %attr = @_;
    my $name = $attr{'name'};
    my $family = $expat->{Document}{'families'}{$name} 
      ||= TAG::Family->new( 'name' => $name , trees => [] );
    $expat->push($family);
}

sub family_ {
    shift->pop;
}

## tmp: to be removed
sub singletree {
    my $expat = shift;
    $expat->push('singletree');    
}

## tmp: to be removed
sub singletree_ {
    shift->pop;
}

sub tree {
    my $expat = shift;
    shift;
    my %attr = @_;
    my $name = $attr{'name'};
    my $family = $expat->{Current};
    my $tree = $expat->{Document}{'trees'}{$name} 
      || TAG::Tree->new( { 'name' => $name, type=> 'initial' , family => [], reducers => []});
    if ($family && $family eq 'singletree') { ## tmp : to be removed
	push( @{$tree->{'family'}}, $name );
	$expat->{Document}{'families'}{$name} = TAG::Family->new( 'name' => $name, 
								  'trees' => [ $name ] );
    } elsif ($family && blessed $family && $family->isa('TAG::Family')) {
	push( @{$tree->{'family'}}, $family->{'name'} );
	push( @{$family->{'trees'}}, $name );
    } 
    $expat->{Document}{'trees'}{$name}=$tree;
    $expat->{CurrentTree}=$tree;
    $expat->{CurrentAnchors} = [];
    $expat->push($tree);
}

sub tree_ {
    shift->pop();
}

sub ht {
    my $expat = shift;
    $expat->{Where} = 'ht';
}

sub ht_ {
  my $expat = shift;
  undef $expat->{Where};
}

sub treeref {
    my $expat = shift;
    shift;
    my %attr = @_;
    my $name = $attr{'name'};
    my $family = $expat->{Current};
    my $tree = $expat->{Document}{'trees'}{$name} || TAG::Tree->new( { 'name' => $name, type=> 'initial' , family => []});
    if ($family && blessed family && $family->isa('TAG::Family')) {
	push( @{$tree->{'family'}}, $family->{'name'} );
	push( @{$family->{'trees'}}, $name );
    }
    $expat->{Document}{'trees'}{$name}=$tree;
}

sub treeref_ {
}

sub TAG::Node::normalize {
    my $self = shift;
    if (!defined $self->{'type'}) {
	if (defined $self->{'lex'}) {
	    $self->{'type'}='lex';
	} elsif (@{$self->{'child'}}) {
	    $self->{'type'}='std';
	} else {
	    $self->{'type'}='subst';
	}
    }
    if (!defined $self->{'adj'}) {
	if (@{$self->{'child'}} || $self->{'type'} eq 'anchor' || $self->{'type'} eq 'coanchor') {
	    $self->{'adj'} = 'yes';
	} else {
	    $self->{'adj'} = 'no';
	}
    }
}

sub optional {
  my $self = shift;
  shift;
  my %attr = @_;
  my $n = $attr{'n'} || 0;
  $self->push_node(TAG::Optional->new({n => $n, guards => []}));
}

sub optional_ {
  my $expat = shift;
  my $opt = $expat->{Current};
  if (($opt->{n} eq '*') && @{$opt->{node}{guards}}) {
    $opt->{guards} = $opt->{node}{guards};
    $opt->{node}{guards} = [];
  }
  $expat->pop();
}

sub interleave {
  shift->push_node(TAG::Interleave->new({child => [], guards => []}));
}

sub interleave_ {
  shift->pop();
}
  
sub sequence {
  shift->push_node(TAG::Sequence->new({child => [], guards => []}));
}

sub sequence_ {
  shift->pop();
}

sub alternative {
  shift->push_node(TAG::Alternative->new({child => [], guards => []}));
}

sub alternative_ {
  shift->pop();
}


sub node {
    my $expat = shift;
    shift;
    my $node = TAG::Node->new( { child => [], guards => [], @_ } );
    if ($node->{'type'} eq 'anchor'){
      $expat->{CurrentTree}{anchor} = $node;
      my $anchors = $expat->{CurrentAnchors};
      if ($node->{'cat'} =~ /^V/i) { 
	# assume Verbs are principal anchors
	# maybe false !
	# should display a warning (mode verbose)
	foreach my $anchor (@$anchors) {
	  $anchor->{'type'}='subst';
	  $anchor->{'adj'}='no';
	}
	$expat->{CurrentAnchors} = [ $node ];
      } elsif (@$anchors) {
	$node->{'type'}='subst';
	$node->{'adj'}='no'
      } else {
	push(@$anchors,$node);
      }
    }
    $expat->{CurrentTree}{'type'} = 'auxiliary' if ($node->{'type'} eq 'foot');
    $expat->push_node($node);
}

sub node_ {
    my $expat = shift;
    $expat->{Current}->normalize;
    $expat->pop();
}

sub narg {
  my $expat = shift;
  shift;
  my %attr = @_;
  my $node = $expat->{Current};
  my $type = $attr{'type'};
  my $comp = TAG::Narg->new({ name => $type, value => [] });
  $node->{$type} = $comp;
  $expat->push($comp);
  $expat->{Where} = 'value';
}

sub narg_ {
  my $expat = shift;
  $expat->pop();
  undef $expat->{Where};
}


sub guards {
  my $expat = shift;
  shift;
  my $guard = TAG::NGuards->new({@_,'guards' => [] });
  push(@{$expat->{Current}{guards}},$guard);
  $expat->push($guard);
}

sub guards_ {
  shift->pop();
}

sub and {
  my $expat = shift;
  shift;
  my $and = TAG::And->new({@_,'guards' => [] });
  push(@{$expat->{Current}{guards}},$and);
  $expat->push($and);
}

sub and_ {
  shift->pop();
}


sub guard {
    my $expat = shift;
    shift;
    my $guard = TAG::NGuard->new({@_,'values' => [] });
    push(@{$expat->{Current}{guards}},$guard);
    $expat->push($guard);
    $expat->{Where} = 'values';
}

sub guard_ {
  my $expat = shift;
  $expat->pop();
  undef $expat->{Where};
}

sub xguard {
    my $expat = shift;
    shift;
    my $guard = TAG::NXGuard->new({@_,'values' => [] });
    push(@{$expat->{Current}{guards}},$guard);
    $expat->push($guard);
    $expat->{Where} = 'values';
}

sub xguard_ {
  my $expat = shift;
  $expat->pop();
  undef $expat->{Where};
}

sub or {
  my $expat = shift;
  shift;
  my $or = TAG::Or->new({@_,'guards' => [] });
  push(@{$expat->{Current}{'guards'}},$or);
  $expat->push($or);
}

sub or_ {
  shift->pop();
}

sub description {
    my $expat = shift;
#    my $desc = TAG::Desc->new({feature => []});
    my $desc = TAG::Desc->new({name => 'desc', value => []});
    $expat->{Current}{'desc'}=$desc;
    $expat->push($desc);
    $expat->{Where} = 'value';
}

sub description_ {
  my $expat = shift;
  $expat->pop();
  undef $expat->{Where};
}

package XML::Parser::Expat;
use UNIVERSAL;
use Scalar::Util qw{blessed reftype};

sub push {
    my ($expat,$obj) = @_;
    push @{ $expat->{Stack} }, $expat->{Current};
    $expat->{Current} = $obj;
}

sub pop {
    my $expat = shift;
    $expat->{Current} = pop @{ $expat->{Stack} };
}

sub push_node {
  my $expat = shift;
  my $node = shift;
  my $current = $expat->{Current};
  if (blessed $current && $current->isa('TAG::Tree')) {
    $current->{'root'} = $node;
  } elsif (blessed $current && $current->isa('TAG::Optional')) {
    $current->{node} = $node;
  } else {
    CORE::push @{ $current->{'child' } }, $node;
  }
  $expat->push($node);
}

sub push_val {
  my $expat = shift;
  my $val = shift;
  my $where = $expat->{Where} || shift || 'value';
  if (defined $expat->{Current}{$where} 
      && (reftype($expat->{Current}{$where}) eq "ARRAY")) {
    CORE::push(@{$expat->{Current}{$where}},$val);
  } else {
    $expat->{Current}{$where} = $val;
  }
  return $where;
}

1;

__END__

=head1 NAME

TAG::XML::Parser - an XML event parser for TAG objects

=head1 SYNOPSIS

 use TAG::XML::Parser;
    
 my $parser = TAG::XML::Parser->new( [OPTIONS] );

 my $doc = $parser->parsefile( <file> [,OPTIONS] );
 or 
 my $doc = $parser->parse( <string>, [,OPTIONS] );

=head1 DESCRIPTION

This module provides ways to parse Tree Adjoining Grammars that are in
XML format. The parser is just a subclass of a an
`C<XML::Parser>' which returns a `C<TAG::Document>' object.

=head1 EXAMPLES

Here is an example of parsing a TAG in XML format:

    use TAG::XML::Parser;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)
XML::Parser (3)

Extensible Markup Language (XML) <http://www.w3c.org/XML>

=cut
