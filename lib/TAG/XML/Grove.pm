# $Id$
package TAG::XML::Grove;

require 5.005;
use strict;
use warnings;
use TAG;
use XML::Grove;

sub iso {
    my $string = shift || "";
#    $string =~ tr/\0-\x{ff}//UC;
    return $string;
}

sub TAG::Document::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),
		      @{ $self->{'desc'} },
		      values %{ $self->{'trees'} },
		      values %{ $self->{'families'} },
		      values %{ $self->{'lexicon'} },
		      map( values %{ $self->{'lemma'}{$_} }, values %{ $self->{'lemma'} }),
		      );
    my $tag = XML::Grove::Element->new ( Name => 'tag',
					 Contents => [ @content ] );
    return XML::Grove::Document->new ( Contents => [ $tag ] );
}


sub TAG::Desc::as_xml_grove {
    my $self = shift;
    my @content = XML::Grove::Characters->new( Data => $self->{'text'});
    return XML::Grove::Element->new ( Name => 'desc',
				      Contents => [ @content ] );
}

sub TAG::Family::as_xml_grove {
    my $self = shift;
    my @content = map( XML::Grove::Element->new( Name => 'treeref',
						 Attributes => { 'name' => $_ },
						 Content => [] ),
		       @{ $self->{'trees'} }
		       );
    return XML::Grove::Element->new( Name => 'family',
				     Attributes => { 'name' => $self->{'name'} },
				     Contents => [ @content ]
				     );
}

sub TAG::Lexicon::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),
		      @{ $self->{'desc'}},
		      @{ $self->{'lemma'}});
    return XML::Grove::Element->new ( Name => 'morph',
				      Attributes => { 'lex' => $self->{'lex'} },
				      Contents => [ @content ] );
}

sub TAG::LemmaRef::as_xml_grove {
    my $self = shift;
    my $feature = $self->{'feature'};
    my @content = ();
    push(@content,$feature->as_xml_grove()) if ($feature);
    return XML::Grove::Element->new ( Name => 'lemmaref',
				      Attributes => { 'name' => $self->{'name'},
						      'cat' => $self->{'cat'}
						  },
				      Contents => [ @content ] );
}

sub TAG::Lemma::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),
		      @{ $self->{'desc'} },
		      @{ $self->{'anchors'} });
    return XML::Grove::Element->new ( Name => 'lemma',
				      Attributes => { 'name' => $self->{'name'},
						      'cat' => $self->{'cat'},
						  },
				      Contents => [ @content ] );
}

sub TAG::Anchor::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),
		      @{ $self->{'coanchors'} },
		      @{ $self->{'equations'} }
		      );
    return XML::Grove::Element->new ( Name => 'anchor',
				      Attributes => { 'tree_id' => join(' ',@{$self->{'family'}})
						  },
				      Contents => [ @content ] );
}

sub TAG::Coanchor::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),@{ $self->{'lex'} });
    return XML::Grove::Element->new ( Name => 'coanchor',
				      Attributes => { 'node_id' => $self->{'node_id'}
						  },
				      Contents => [ @content ] );
}

sub TAG::Lex::as_xml_grove {
    my $self = shift;
    my @content = XML::Grove::Characters->new( Data => $self->{'text'});
    return XML::Grove::Element->new ( Name => 'lex',
				      Contents => [ @content ] );
}

sub TAG::Equation::as_xml_grove {
    my $self = shift;
    my $feature = $self->{'feature'};
    my @content = ();
    push(@content,$feature->as_xml_grove()) if ($feature);
    return XML::Grove::Element->new ( Name => 'equation',
				      Attributes => { 'node_id' => $self->{'node_id'},
						      'type' => $self->{'type'}
						  },
				      Contents => [ @content ] );
}

sub TAG::Feature::as_xml_grove {
    my $self = shift;
    my @content = map $self->{'f'}{$_}->as_xml_grove, sort (keys %{ $self->{'f'} });
    return XML::Grove::Element->new ( Name => 'fs',
				      Contents => [ @content ] );
}

sub TAG::Comp::as_xml_grove {
    my $self = shift;
    my @content = map $_->as_xml_grove, @{ $self->{'value' } };
    my %attr = ( 'name' => $self->{'name'} );
    $attr{'id'} = $self->{'id'} if (defined $self->{'id'});
    return XML::Grove::Element->new ( Name => 'comp',
				      Attributes => \%attr,
				      Contents => [ @content ] );
}

sub TAG::Val::as_xml_grove {
    my $self = shift;
    my @content = XML::Grove::Characters->new( Data => $self->{'text'});
    return XML::Grove::Element->new ( Name => 'val',
				      Contents => [ @content ] );
}

sub TAG::Plus::as_xml_grove {
    return XML::Grove::Element->new ( Name => 'plus',
				      Contents => [] );
}

sub TAG::Minus::as_xml_grove {
    return XML::Grove::Element->new ( Name => 'minus',
				      Contents => [] );
}

sub TAG::Tree::as_xml_grove {
    my $self = shift;
    my @content = map( $_->as_xml_grove(), 
		       @{ $self->{'desc'} },
		       $self->{'root'});
    return XML::Grove::Element->new ( Name => 'tree',
				      Attributes => { 'name' => $self->{'name'} },
				      Contents => [ @content ] );
}

our @attr = ( 'id', 'type', 'adj', 'lex', 'cat' );

sub TAG::Node::as_xml_grove {
    my $self = shift;
    my @content = map($_->as_xml_grove(),@{ $self->{'child'} });
    unshift(@content,
	  XML::Grove::Element->new ( Name => 'narg',
				     Attributes => { 'type' => 'top' },
				     Contents => [ $self->{'top'}->as_xml_grove() ]
				     )) if ($self->{'top'});
    unshift(@content,
	  XML::Grove::Element->new ( Name => 'narg',
				     Attributes => { 'type' => 'bot' },
				     Contents => [$self->{'bot'}->as_xml_grove()]
				     )) if ($self->{'bot'});
    my $node = XML::Grove::Element->new ( Name => 'node',
					  Contents => [ @content ] );
    foreach my $attr (@attr) {
	$node->{Attributes}{$attr} = $self->{$attr} if ($self->{$attr});
    }
    return $node;
}

1;

__END__

=head1 NAME

TAG::XML::Grove - the XML::Grove representation of a TAG Grammar

=head1 SYNOPSIS

 use TAG::XML::Grove;
 
 my $grove = $tag_object->as_xml_grove();   

=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)
XML::Grove (3)

Extensible Markup Language (XML) <http://www.w3c.org/XML>

=cut
