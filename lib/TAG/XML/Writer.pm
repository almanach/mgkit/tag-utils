# $Id$
package TAG::XML::Writer;

require 5.005;
use strict;
use warnings;
use TAG;
use XML::Generator;
##use bytes;

our $generator;
our $done;

our %default = ( 'family' => 'familyfile',
		 'lex' => 'lexfile',
		 'lemma' => 'lemmafile'
		 );

sub TAG::Document::pretty_print {
    my $self=shift;
    $generator= new XML::Generator(':pretty');
    $done = {};
    $done->{'info'} = shift;
    my @entries = ();
    push(@entries,map($_->pretty_print,@{$self->{'desc'}}));
    foreach my $family (sort (keys %{ $self->{'families'} })) {
	push(@entries,$self->{'families'}{$family}->pretty_print($self));
    }
    foreach my $lemma (sort (keys %{ $self->{'lemma'} })) {
	my $tmp = $self->{'lemma'}{$lemma};
	foreach my $cat (sort (keys %{ $tmp })) {
	    push(@entries,$tmp->{$cat}->pretty_print);
	}
    }
    foreach my $lexicon (sort (keys %{ $self->{'lexicon'} })) {
	push(@entries,$self->{'lexicon'}{$lexicon}->pretty_print);
    }
    my %tag_attr = ('axiom' => ($self->{'axiom'} || $done->{'info'}{'map'}{'axiom'}));
    foreach my $default (keys %{ $done->{'info'}{'map'}{'default'} }) {
	$tag_attr{$default{$default}} = $done->{'info'}{'map'}{'default'}{$default};
    }
    return $generator->tag(\%tag_attr,@entries);
}

sub TAG::Family::pretty_print {
    my $self=shift;
    my $doc = shift;
    my @content = ();
    map( $generator->treeref( {'name'=> $_ } ),
	 @{ $self->{'desc'}});
    foreach my $tree (sort @{ $self->{'trees'}}){
	if ($done->{'trees'}{$tree} ||
	    ! defined $doc->{'trees'}{$tree}{'root'}
	    ) {
	    push(@content, $generator->treeref( {'name'=> $tree } ));
	} else {
	    push(@content, $doc->{'trees'}{$tree}->pretty_print);
	    $done->{'trees'}{$tree} = $self->{'name'};
	}
    }
    return $generator->family({'name'=> $self->{'name'} },@content);
}

sub TAG::Tree::pretty_print {
    my $self = shift;
    my @content = map( $_->pretty_print, 
		       @{ $self->{'desc'} },
		       @{ $self->{'reducers'} },
		       $self->{'root'});
    return $generator->tree({ 'name' => $self->{'name'} },@content);
} 


sub TAG::Reducer::pretty_print {
  my $self = shift;
  return $generator->reducer($self->{value}[0]->pretty_print);
}

our @attr = ( 'id', 'type', 'adj', 'lex', 'cat' );

sub TAG::Node::pretty_print {
    my $self=shift;
    my @content = map( $_->pretty_print, 
		       @{$self->{'guards'}},
		       @{$self->{'child'}});
    my %attributes = ();
    unshift(@content,
	    $generator->narg( { 'type' => 'top' },$self->{'top'}->pretty_print))
      if ($self->{'top'});
    unshift(@content,
	    $generator->narg( { 'type' => 'bot' },
			      $self->{'bot'}->pretty_print))
      if ($self->{'bot'});
    foreach my $attr (@attr) {
      $attributes{$attr} = $self->{$attr} if ($self->{$attr});
    }
    return $generator->node( \%attributes, @content );
}

sub TAG::Desc::pretty_print {
    my $self = shift;
    return $generator->description(map($_->pretty_print,@{$self->{feature}}));
}

sub TAG::Lexicon::pretty_print {
    my $self = shift;
    my @content = map($_->pretty_print,
		      @{ $self->{'desc'}},
		      @{ $self->{'lemma'}});
    return $generator->morph({ 'lex' => $self->{'lex'} },@content);
}

sub TAG::LemmaRef::pretty_print {
    my $self = shift;
    my $feature = $self->{'feature'};
    my @content = ();
    my $name = $self->{'name'};
    my %attr = ( 'name' => $name,
		 'cat' => $self->{'cat'} );
    push(@content,$feature->pretty_print) if ($feature);
    $attr{'lemmafile'} = $done->{'info'}{'map'}{'lemma'}{$name} if (defined $done->{'info'}{'map'}{'lemma'}{$name});
    return $generator->lemmaref(\%attr,@content);
}

sub TAG::Lemma::pretty_print {
    my $self = shift;
    my @content = map($_->pretty_print,
		      @{ $self->{'desc'} },
		      @{ $self->{'anchors'} });
    return $generator->lemma({ 'name' => $self->{'name'},
			       'cat' => $self->{'cat'} },
			     @content);
}

sub family_pointer {
    my $family = shift;
    my $post = "family[\@name=$family]";
    my $file = $done->{'info'}{'map'}{'family'}{$family};
    return "$file#$post" if ($file);
    return "$post";
}

sub TAG::Anchor::pretty_print {
    my $self = shift;
    my @content = map($_->pretty_print,
		      @{ $self->{'coanchors'} },
		      @{ $self->{'equations'} }
		      );
    my @families = map &family_pointer($_), @{$self->{'family'}};
    return $generator->anchor( { 'tree_id' => join(' ',@families) },
			       @content );
}

sub TAG::Coanchor::pretty_print {
    my $self = shift;
    my @content = map($_->pretty_print,@{ $self->{'lex'} });
    return $generator->coanchor( {'node_id' => $self->{'node_id'}},
				 @content);
}

sub TAG::Lex::pretty_print {
    my $self = shift;
    my @content = ($self->{'text'});
    return $generator->lex(@content);
}

sub TAG::Equation::pretty_print {
    my $self = shift;
    my $feature = $self->{'feature'};
    my @content = ();
    push(@content,$feature->pretty_print) if ($feature);
    return $generator->equation({ 'node_id' => $self->{'node_id'},
				  'type' => $self->{'type'}},
				@content
			       );
}

sub TAG::Feature::pretty_print {
    my $self = shift;
    my @content = map $self->{'f'}{$_}->pretty_print, sort (keys %{ $self->{'f'} });
    return $generator->fs(@content);
}

sub TAG::Comp::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'value'} };
    my %attr = ( 'name' => $self->{'name'} );
    @content = ($generator->vAlt(@content)) if (@content > 1);
    return $generator->f(\%attr,@content);
}

sub TAG::Var::pretty_print {
  my $self = shift;
  my @content = map $_->pretty_print, @{ $self->{'value'} };
  @content = ($generator->vAlt(@content)) if (@content > 1);
  return $generator->var({name=>$self->{'name'}},@content);
}

sub TAG::VAlt::pretty_print {
  my $self = shift;
  my @content = map $_->pretty_print, @{ $self->{'value'} };
  return $generator->vAlt(@content);
}

sub TAG::Not::pretty_print {
  my $self = shift;
  my @content = map $_->pretty_print, @{ $self->{'value'} };
  @content = ($generator->vAlt(@content)) if (@content > 1);
  return $generator->not(@content);
}

sub TAG::Narg::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'value'} };
    @content = ($generator->vAlt(@content)) if (@content > 1);
    return @content;
}


sub TAG::NGuards::pretty_print {
  my $self = shift;
  my @content = map $_->pretty_print, @{ $self->{'guards'} };
  return $generator->guards({rel=>$self->{rel}},@content);
}

sub TAG::Or::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'guards'} };
    return $generator->or(@content);
}

sub TAG::And::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'guards'} };
    return $generator->and(@content);
}

sub TAG::NGuard::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'values'} };
    my %attr = ();
    $attr{type} = $self->{type} if (exists $self->{type});
    return $generator->guard(\%attr,@content);
}


sub TAG::Sequence::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print,@{$self->{'guards'}},@{ $self->{'child'} };
    return $generator->sequence(@content);
}

sub TAG::Alternative::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print,@{$self->{'guards'}},@{ $self->{'child'} };
    return $generator->alternative(@content);
}

sub TAG::Interleave::pretty_print {
    my $self = shift;
    my @content = map $_->pretty_print, @{ $self->{'child'} };
    return $generator->interleave(@content);
}

sub TAG::Optional::pretty_print {
  my $self = shift;
  return $generator->optional($self->node->pretty_print);
}

sub TAG::Val::pretty_print {
    my $self = shift;
    my $content = $self->{'text'};
    return $generator->symbol({value=>$content});
}

sub TAG::Plus::pretty_print {
    return $generator->plus();
}

sub TAG::Minus::pretty_print {
    return $generator->minus();
}

1;

__END__

=head1 NAME

TAG::XML::Writer - Pretty-print the XML representation of a Tree Adjoining Grammar

=head1 SYNOPSIS

 use TAG::XML::Parser;
 
 print $tag_object->pretty_print();   

=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

TAG DTD <http://alpage.inria.fr/~clerger/tag.dtd,xml>

Extensible Markup Language (XML) <http://www.w3c.org/XML>

=cut
