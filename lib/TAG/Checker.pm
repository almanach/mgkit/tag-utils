# $Id$
package TAG::Checker;

require 5.005;
use strict;
use warnings;
use TAG;

our @ISA         = qw(Data::Grove);
our $stats;

sub iso {
    my $string = shift || "";
#    $string =~ tr/\0-\x{ff}//UC;
    return $string;
}

sub msg {
    my $type = shift;
    print "*** @_\n";
    $stats->{'errors'}++;
    $stats->{'error'}{$type}++;
}

sub TAG::Document::check {
    my $self = shift;
    $stats = TAG::Checker->new( 'error' => { 'lexicon' => 0,
					     'lemma' => 0,
					     'family' => 0
					     },
				'errors' => 0,
				'lexicon' => 0,
				'lemma' => 0,
				'families' => 0,
				'trees' => 0,
				);
    $_->check($self) foreach (values %{$self->{'lexicon'}});
    foreach my $lemma (values %{$self->{'lemma'}}) {
	$_->check($self) foreach (values %$lemma);
    }
    $_->check($self) foreach (values %{$self->{'families'}});
    print <<EOF;
Statistics:  Entries      Errors
\tlexicon:   $stats->{'lexicon' }    $stats->{'error'}{'lexicon'}
\tlemma:     $stats->{'lemma'   }    $stats->{'error'}{'lemma'}
\tfamilies:  $stats->{'families'}     $stats->{'error'}{'family'}
\ttrees:     $stats->{'trees'   }
\tTotal:            $stats->{'errors'}

EOF
    print "\nDistrib: #families with #trees   \n";
    foreach my $n (sort {$a <=> $b} keys %{$stats->{'distrib'}{'trees'}}) {
	print "\t$stats->{'distrib'}{'trees'}{$n}\t$n\n";
    }
    print "\nDistrib: #lemma with #anchors   \n";
    foreach my $n (sort {$a <=> $b} keys %{$stats->{'distrib'}{'lemma'}}) {
	print "\t$stats->{'distrib'}{'lemma'}{$n}\t$n\n";
    }
    print "\nDistrib: #lexicon with #lemmaref   \n";
    foreach my $n (sort {$a <=> $b} keys %{$stats->{'distrib'}{'lexicon'}}) {
	print "\t$stats->{'distrib'}{'lexicon'}{$n}\t$n\n";
    }
}


sub TAG::Family::check {
    my $self = shift;
    my $doc = shift;
    my $family = $self->{'name'};
    $stats->{'families'}++;
    my @trees = sort @{$self->{'trees'}};
    $stats->{'trees'} += @trees;
    $stats->{'distrib'}{'trees'}{@trees}++;
#	print "Family $family: @trees\n";
    &msg('family',
	 "No associated trees for family $family") unless( @trees );
    my $x = $stats->{'anchor_lemma'}{$family} || "";
    &msg('family',
	 "No anchored lemma for family $family") unless( "$x" eq 'yes' );
}

sub TAG::Lexicon::check {
    my $self = shift;
    my $doc = shift;
    my @lemmaref = @{$self->{'lemma'}};
    my $lex = $self->{lex};
    my $iso_lex = &iso($lex);
#    print "Lexicon $iso_lex\n";
    $stats->{'lexicon'}++;
    &msg('lexicon',
	 "Missing lexical form for lexicon entry"),return unless ($lex);
    &msg('lexicon',
	 "Missing associated lemma for $iso_lex"),return unless (@lemmaref);
    $stats->{'distrib'}{'lexicon'}{@lemmaref}++;
    $_->check($lex,$doc) foreach (@lemmaref);
}

sub TAG::LemmaRef::check {
    my $self = shift;
    my $lex = shift;
    my $doc = shift;
    my $name = $self->{'name'};
    my $cat = $self->{'cat'};
    my $lexicon = "in lexicon entry '" . &iso($lex) ."'";
#    print "\tLemma '". &iso($name) . "'/". &iso($cat) . "\n";
    &msg('lexicon',
	 "Missing name or category for lemma entry"),return unless ($name && $cat);
    my $lemma = $doc->{'lemma'}{"$name"}{$cat};
    &msg('lexicon',
	 "Bad lemma reference " . &iso($name) . "/$cat $lexicon"), return unless ($lemma);
    &msg('lexicon',
	 "Category mismatch with $lemma->{cat} $lexicon"), return unless ("$cat" eq "$lemma->{cat}");
    $lemma->{'has_lexicon'}++;
}

sub TAG::Lemma::check {
    my $self = shift;
    my $doc = shift;
    my $name = &iso($self->{'name'});
    my $cat = &iso($self->{'cat'});
    my $ll = "Lemma '$name'/$cat\n";
#    print "Lemma '$name'/$cat\n";
    $stats->{'lemma'}++;
    &msg('lemma',
	 "Missing name or category in $ll"), return unless ($name && $cat);
    &msg('lemma',
	 "No associated lexicon entries for $ll"), return unless ($self->{'has_lexicon'});
    my @anchors = @{ $self->{'anchors'} };
    &msg('lemma',
	 "Missing anchors in $ll\n"),return unless (@anchors);
    $stats->{'distrib'}{'lemma'}{@anchors}++;
    $_->check($ll,$doc) foreach (@anchors);
}

sub TAG::Anchor::check {
    my $self = shift;
    my $ll = shift;
    my $doc = shift;
    my @family = @{ $self->{'family'} };
#    print "\tAnchor @family\n";
    &msg('lemma',
	 "Missing family info in $ll"),return unless (@family);
    foreach my $family (@family) {
	&msg('lemma',
	     "No family $family in $ll"), next unless ($doc->{'families'}{$family});
	$stats->{'anchor_lemma'}{$family}='yes';
    }
}

1;

__END__

=head1 NAME

TAG::Checker

=head1 SYNOPSIS

 use TAG::Checker;
    
=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
