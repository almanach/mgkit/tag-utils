# $Id$
package TAG::XRCG;

require 5.005;
use strict;
use warnings;
use TAG;
use TAG::LP;

#our $treenb=1;
our $nodes=0;
our %info=();
our $trees=1;

sub TAG::Document::XRCG {
    my $self = shift;
    my $map = shift || {};
    $info{'doc'}=$self;
    $info{'adj'}={};
    my @includes = ();
    # Reading header file if specified for finite set declarations
    if (defined $map->{'lp'}{'header'}){
      TAG::LP::header_reader($map->{'lp'}{'header'});
	print <<EOF;

:-include '$map->{'lp'}{'header'}'.

:-op(300,xfy,[@]).

:-tagop(':').

:-std_prolog rcg_anchor/6.

rcg_anchor(L:R,Cat,Family,Top,Tree,Token) :-
    anchor(tag_anchor{ name => Family },
	   Token,
	   Cat,
	   L,
	   R,
	   Top).

:-extensional empty_adj/4.

empty_adj(X,X,A:A,B:B).

EOF
    }
    my @rules = ();
    my $axiom =  $self->{'axiom'};
    $axiom = TAG::LP::lower_or_quote($axiom);
    my $non_terminal = $axiom. complete_non_terminal($axiom);
    push(@rules, <<EOF);

%% XRCG $TAG::XRCG::trees: Axiom $self->{'axiom'}
?-recorded('N'(N)),rcg_phrase(Axiom::$non_terminal (0:N)).

EOF
    $TAG::XRCG::trees++;
    push(@rules,
	 map($self->{'families'}{$_}->XRCG($self),
	     sort (keys %{$self->{'families'}})));
    foreach my $lemmakey (sort (keys %{$self->{'lemma'}})) {
        my $tmp = $self->{'lemma'}{$lemmakey};
        foreach my $cat (sort (keys %$tmp )){
            push(@rules,$tmp->{$cat}->LP());
        }
    }
    foreach my $lexkey (sort (keys %{$self->{'lexicon'}})) {
	push(@rules,$self->{'lexicon'}{$lexkey}->LP());
    }
    push(@rules,"\n");
    return @rules;
}

sub TAG::Document::XRCGlex {
    my $self = shift;
    $info{'doc'}=$self;
    return  map($self->{'lexicon'}{$_}->XRCGlex,
		sort (keys %{$self->{'lexicon'}}));
}

sub TAG::Family::XRCG {
    my $self = shift;
    my $doc = shift;
    my $family = $self->{'name'};
    return map($doc->{'trees'}{$_}->XRCG($family),
	       sort @{$self->{'trees'}});
}

sub TAG::Tree::XRCG {
    my $self = shift;
    my $family = shift;
    my $root= $self->{'root'};
    my $cat = TAG::LP::lower_or_quote($root->{'cat'});
    my $tree_name = $self->{'name'};
    my $top = $root->{'top'};
    my $bot = $root->{'bot'};
    $top = $cat . ($top ? $top->LP($cat) : complete_non_terminal($cat));
    $bot = $cat . ($bot ? $bot->LP($cat) : complete_non_terminal($cat));
    my $prefix = "TOP";
    my $label = $self->{'type'} eq 'initial' ? "($prefix\::$top)" : "adj_$cat($prefix\::$top,Foot)";
    my ($head,$body) = $root->XRCG($family,$tree_name,$prefix);
    my $treenb = $TAG::XRCG::trees++;
    $TAG::XRCG::nodes=0;
    $info{'adj'}{$root->{'cat'}} = 1 if ($self->{'type'} eq 'initial');
    return <<EOF ;
%% XRCG $treenb: Tree $self->{'name'}
$label ($head) -->
\t$body.
EOF
}

sub complete_non_terminal {
    my $cat = shift;
    return (defined $TAG::LP::features{$cat} ? '{}' : "");
}

sub TAG::Node::XRCG {
    my $self = shift;
    my $family = shift;
    my $tree_name = shift;
    my $prefix = shift;
    my $type = $self->{'type'};
    my $id = $self->{'id'};
    my $top = $self->{'top'};
    my $bot = $self->{'bot'};
    my $cat = TAG::LP::lower_or_quote($self->{'cat'});
    $top = $prefix || ($cat . ($top ? $top->LP($cat) : complete_non_terminal($cat)));
    $bot = $cat . ($bot ? $bot->LP($cat) : complete_non_terminal($cat));
    $id = $TAG::XRCG::nodes++ unless ($id);
    my $qid = TAG::LP::quote($id);
    if ($type eq 'foot') {
	$id = $TAG::XRCG::nodes++ unless ($id);
	return ( ',' , "{ Foot = $bot }" ) 
    }
    if ($type eq 'lex') {
	my $lex = $self->{'lex'};
	$lex = "[".TAG::LP::quote($lex)."]" if ($lex);
	return (  $lex , '' ) ;
    }
    if ($type eq 'subst') {
	$id = $TAG::XRCG::nodes++ unless ($id);
	my $var = "S$id";
	return ( $var, "$qid: $top ($var)" );
    }
    my @head = ();
    my @body = ();
    if ($type eq 'anchor') {
	my $var = "Anchor";
	push(@head,$var);
	push(@body,"{rcg_anchor($var\::L:R,$cat,$family,Anchor_Bot::$bot,$tree_name,Token), '<>': tag_anchor(Token,L,R,$tree_name)}");
	$bot='Anchor_Bot';
    } else {
	foreach my $child (@{$self->{'child'}}) {
	    my ($chead,$cbody) = $child->XRCG($family,$tree_name,'');
	    push(@head,$chead) if ($chead);
	    push(@body,$cbody) if ($cbody);
	}
    }
    if ( $self->{'adj'} eq 'yes' ){
	my $varL = "L$id";
	my $varR = "R$id";
	$info{'adj'}{$cat} = 0 unless (defined $info{$cat});
	unshift @body, "({empty_adj($top,$bot,$varL,$varR) } ; $qid: adj_$cat($top,$bot) ($varL,$varR))";
	unshift @head, $varL;
	push(@head,$varR);
    }
    my $head = join("@",@head);
    $head =~ s/@\s*,/,/o;
    $head =~ s/,\s*@/,/o;
    return ($head, join(",\n\t",@body));
}

sub TAG::Lexicon::XRCGlex {
    my $self = shift;
    my @families = map($_->XRCGlex,@{$self->{'lemma'}});
    return "$self->{lex} { @families }";
}

sub TAG::LemmaRef::XRCGlex {
    my $self = shift;
    my $name = $self->{'name'};
    my $cat = $self->{'cat'};
    my $lemma = $info{'doc'}{'lemma'}{$name}{$cat};
    return map($_->XRCGlex($self->{'cat'}),@{$lemma->{'anchors'}}) if ($lemma);
}

sub TAG::Anchor::XRCGlex {
    my $self = shift;
    my $cat = shift;
    return map("+${_}_$cat",@{$self->{'family'}});
}

1;

__END__

=head1 NAME

TAG::XRCG - output content of a Tree Adjoining Grammar (TAG) as a Range Concatenation Grammar (XRCG)

=head1 SYNOPSIS

 use TAG::XRCG;
 
 # using XRCG method on a TAG::Document 
 @rules = $tag_document->XRCG();

=head1 DESCRIPTION

Calling `C<XRCG>' on an TAG object prints it as an eXtended Range Concatenation
Grammar. 

=head1 OPTIONS

    none.

=over 4

=back

=head1 EXAMPLES

Here is an example of parsing a TAG in XML format and emitting it as a XRCG:

    use TAG::XML::Parser;
    use TAG::XRCG;

    my $parser = TAG::XML::Parser->new ();
    my $doc = $parser->parsefile( $grammarfile );

    print join("\n",$doc->XRCG()),"\n"; 

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
