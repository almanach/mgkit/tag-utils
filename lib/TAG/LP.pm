# $Id$
package TAG::LP;

require 5.005;
use strict;
use warnings;
use TAG;
use TAG::Tools;
use Digest::MD5 qw{md5_hex};

##use bytes;
##use UNIVERSAL qw(isa);

our $map;
our %fset=();
our %features = ();
our %secondary = ();

sub prefix {
    return "\t" x shift;
}

sub lower_or_quote {
    my $string = shift;
    $string = "" unless (defined $string);
    return quote($string) if (defined $map->{lp}{no_normalize});
    $string =~ tr/-/_/;
    return lc($string);
}

sub quote {
    my $string = shift;
    $string = "" unless (defined $string);
    return quote_aux($string) if $string =~ /\s/o;
    return quote_aux($string) if $string =~ /^:+$/o;
    return join('!',map(quote_aux($_),split(/:/,$string)))
}

sub quote_aux {
  my $string = shift;
  $string = "" unless (defined $string);
  return $string if ($string =~ /^[a-zA-Z_]\w*$/o && $string !~ /^[_A-Z]/ );
  return $string if ($string =~ /^\d+$/);
  #    $string =~ s/\'/\'\'/og;
  $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}

sub var {
    my $string = shift;
    $string =~ s/\W/_/og;
    return "_$string";
}

sub single_or_list {
    my $o = shift ;
    if (@{$o} == 1) {
	return $o->[0];
    } else {
	return &list($o,@_);
    }
}

sub list {
    my $o = shift;
    my $format = shift;
    return $format->{'open'} . join( $format->{'sep'}, @$o ) . $format->{'close'}; 
}

sub header_reader {
    my $header = shift;
    open(HEADER,"< $header") || die "can't open header file $header";
    while(<HEADER>) {
	if (/^:-finite_set\(\s*(\S+?)\s*,/) {
#	    print STDERR "SETTING FSET $1\n";
	    $fset{$1}=1;
	}
	if (/^:-features\(\s*(\S+?)\s*,/) {
	  ## print STDERR "SETTING FEATURE $1 from $_\n";
	    $features{$1}=1;
	}
    }
    close(HEADER);
}

sub TAG::Document::LP {
    my $self = shift;
    $map = shift || {};
    %fset=();
    my @includes = ();
    # Reading header file if specified for finite set declarations
    if (defined $map->{'lp'}{'header'}){
	my $header = $map->{'lp'}{'header'};
	header_reader($header);
	push(@includes,$header);
    }
    my @lexkeys = sort (keys %{$self->{'lexicon'}});
    my @lemma = ();
    foreach my $lemmakey (sort (keys %{$self->{'lemma'}})) {
	my $tmp = $self->{'lemma'}{$lemmakey};
	foreach my $cat (sort (keys %$tmp )){
	    push @lemma, $tmp->{$cat};
	}
    }
    my @trees = ();
    my $done = {};
    foreach my $family (sort (keys %{$self->{'families'}})) {
	push(@trees,$self->{'families'}{$family}->LP($self,$done));
    }
    my $s= ( join("\n", map( $_->LP(), @{ $self->{'desc'} }))
	     . "\n"
	     . join("\n",map( ":-include('$_').", @includes)) . "\n"
	     . join("\n",
		    map( ($self->{'lexicon'}{$_})->LP() , @lexkeys  ))
	     . "\n\n"
	     . join("\n\n",
		    map( $_->LP() , @lemma  ))
	     . "\n\n"
	     . join("\n\n",@trees) 
	     . "\n\n"
	     );
##    $s =~ tr/\0-\x{ff}//UC;
##    $s = UTF8toISO($s);
    return $s;
}

sub TAG::Family::LP {
    my $self = shift;
    my $doc = shift;
    my $done = shift;
    my @keys = sort @{$self->{'trees'}};
    my @trees = ();
    push(@trees,"%% Family $self->{'name'}: {" . join(',',@keys) . "}" );
    foreach my $tree (@keys) {
	if ($done->{'trees'}{$tree}) {
	    push(@trees,"%% Tree $tree already emitted for family $done->{'trees'}{$tree}");
	} else {
	    push(@trees,$doc->{'trees'}{$tree}->LP());
	    $done->{'trees'}{$tree} = $self->{'name'};
	}
    }
    return @trees;
}

sub TAG::Desc::LP {
    my $self = shift;
    return "%% $self->{'text'}";
}

sub TAG::Tree::LP {
    my $self = shift;
    %secondary = ();
    my @args = ();
    my $type = $self->{'type'};
    my $name = &TAG::LP::quote($self->{'name'});
    my @family = map TAG::LP::quote($_), @{ $self->{'family'} };
    my @secondaries = ();
    if (exists $self->{desc}) {
      my $desc = $self->{desc}{value}[0];
      if (exists $desc->{f}{secondary}) {
##	print "found secondary for $name\n";
	my $secondary = $desc->{f}{secondary}{value}[0];
	foreach my $x (keys %{$secondary->{f}}) {
	  my $data = $secondary->{f}{$x}{value}[0];
	  (ref($data) eq 'TAG::Var') and $data = $data->{value}[0];
	  my $ref = $data->{f}{ref};
	  my $gov = $data->{f}{gov};
	  my $label = $data->{f}{label2};
	  (defined $ref && defined $gov && defined $label) or next;
	  $ref = $ref->{value}[0]->LP();
	  $gov = $gov->{value}[0]->LP();
	  $label = $label->{value}[0]->LP();
	  $ref =~ s/\s*::.*$//;
	  $gov =~ s/\s*::.*$//;
	  $label =~ s/\s*::.*$//;
	  my $axis = (exists $data->{f}{axis}) ? $data->{f}{axis}{value}[0]->LP() : 'local';
	  $axis =~ s/\s*::.*$//;
##	  print "\tsecondary key $x $data $ref $gov $label\n";
	  push(@secondaries,bless({ id => $x, ref => $ref, gov => $gov, label => $label, axis => $axis },'TAG::Secondary'));
	}
##	print "Secondaries @secondaries\n";
      }
    }
    @family = ($self->{ht}->LP('ht'))
      if ((defined $self->{anchor}) 
	  && defined $map->{'lp'}{'hypertag'} 
	  && defined $self->{ht});
    $type = ($type eq 'initial' ? 'tree' : 'auxtree' );
    push(@args, 
	 "name => " . $name,
	 "family => " . TAG::LP::single_or_list(\@family, { open => '[', close => ']', sep=>',' }),
	 "tree => $type " . $self->{'root'}->LP(3,@secondaries,@{$self->{reducers}})
	 );
    my $output = "tag_tree" . &TAG::LP::list(\@args,
					     { open=> "{\t",
					       close=> "\n\t}",
					       sep=> ",\n\t\t"
					     }) . ".";
    if (exists $self->{'desc'}
	&& @{$self->{desc}{value}}
       ) {
      my $desc = $self->{desc}{value}[0];
      if (exists $desc->{f}{dependency}) {
	my $label = &TAG::LP::quote($desc->{f}{dependency}{value}->[0]{text});
	$output .= "\n\ndependency_label($name,$label).\n\n";
      }
    }
    foreach my $dep (keys %secondary) {
      my $secondary = $secondary{$dep};
      my $head = &TAG::LP::quote($secondary->[0]);
      my $label = &TAG::LP::quote($secondary->[1]);
      $dep = &TAG::LP::quote($dep);
      $output .= "\n\nedge_secondary_constraint($name,$dep,$head,$label).\n\n";
    }
    my $altname = $self->{name};
    $altname =~ s/^\d+\s+//o;
    my $md5 = &TAG::LP::quote(md5_hex($altname));
    $output .= "\n\ntree2md5($name,$md5).\n\n";
    return $output;
}

sub TAG::Optional::LP {
  my $self = shift;
  my $output = $self->{node}->LP(@_);
  my $n = $self->{n};
  my $mark = "?";
  $mark = '@*' if ($n eq '*');
  if (($n eq '*') && @{$self->{guards}}) {
    ## $self is a star node and guards have been set on star rather than
    ## on the embeded node (thanks to XML/Parser.pm)
    return guard_wrapper($self,"($output)$mark");
  } else {
    return @{$self->{node}{guards}} ? "$output" : "($output)$mark";
  }
}

sub guard_wrapper {
  my $self = shift;
  my $display = shift;
  if (@{$self->{guards}}) {
    my %guards = map($_->LP,@{$self->{guards}});
    my $plus = $guards{'+'} || 'true';
    my $minus = $guards{'-'} || 'true';
    $display = "guard{ goal => $display, plus => ( $plus ), minus => ( $minus ) }";
  }
  return $display;
}

sub TAG::Interleave::LP {
  my $self = shift;
  my $depth = shift;
  my @reducers = @_;
  my @child = map($_->LP($depth+1),@{$self->{child}});
  my $display = &TAG::LP::list( \@child,
			       { open => "(\n" . TAG::LP::prefix($depth+1), 
				 close => "\n" . TAG::LP::prefix($depth) . ")", 
				 sep => "\n" . TAG::LP::prefix($depth) . "##      " });
  if (@reducers) {
    my $postprocess = join(', ',map {$_->LP($depth+1)} @reducers);
    $display = "($display,\{$postprocess\})";
  }
  return guard_wrapper($self,$display);
}

sub TAG::Sequence::LP {
  my $self = shift;
  my $depth = shift;
  my @reducers = @_;
  my @tmp = @{$self->{child}};
  my @child = ();
  while (@tmp) {
    my $first = shift @tmp;
    if (@reducers && !@tmp) {
      push(@child,$first->LP($depth+1,@reducers));
    } else {
      push(@child,$first->LP($depth+1));
    }
  }
  my $display = &TAG::LP::list( \@child,
				{ open => "(\n" . TAG::LP::prefix($depth+1), 
				  close => "\n" . TAG::LP::prefix($depth) . ")", 
				  sep => "\n" . TAG::LP::prefix($depth) . ",       " });
  return guard_wrapper($self,$display);
}

sub TAG::Alternative::LP {
  my $self = shift;
  my $depth = shift;
  my @reducers = @_;
  my @child = map($_->LP($depth+1),@{$self->{child}});
  my $display = &TAG::LP::list( \@child,
				{ open => "(\n" . TAG::LP::prefix($depth+1), 
				  close => "\n" . TAG::LP::prefix($depth) . ")", 
				  sep => "\n" . TAG::LP::prefix($depth) . ";       " });
  if (@reducers) {
    my $postprocess = join(', ',map {$_->LP($depth+1)} @reducers);
    $display = "($display,\{$postprocess\})";
  }
  return guard_wrapper($self,$display);
}


sub TAG::Node::LP {
    my $self = shift;
    my $depth = shift;
    my @reducers = @_;		#  could also be secondaries
    my $cat = TAG::LP::lower_or_quote($self->{'cat'});
    my $type = $self->{'type'};
    my $lex = $self->{'lex'};
    my $adj = $self->{'adj'};
    my $adjleft = $self->{'adjleft'};
    my $adjright = $self->{'adjright'};
    my $adjwrap = $self->{'adjwrap'};
    my $id = $self->{'id'};
    my $top = $self->{'top'};
    my $bot = $self->{'bot'};
    my $token = $self->{token} || $self->{lex};
    my @child = @{$self->{'child'}};
    my $child='';
    my @args = ();
    my @postprocess=();
    if ($id && $type ne 'skip') {
      my $varid='';
      my $ref;
      if ($id =~ /^\^\s+(\S+)\s+\^\s*/) {
	$varid = TAG::LP::var($1);
	$ref = $varid;
	$id =~ s/^\^\s+(\S+)\s+\^\s*//o;
	$id and $varid .= '::';
      }
      my @ids=map {TAG::LP::quote($_)} split(/\s+/,$id);
      my $nodeid = $map->{lp}{nodeid_set} ||= 'nodeid';
      if (scalar(@ids) > 1) {
	push(@args,"id= ${varid}${nodeid}[".join(',',@ids)."]");
      } else {
	push(@args,"id= ${varid}$ids[0]");
	$ref = $ids[0];
      }
      if (exists $self->{secondary}) {
	my $secondary  = $self->{secondary};
	my $var='';
	if ($secondary =~ /^\^\s+(\S+)\s+\^\s*/) {
	  $var = TAG::LP::var($1);
	  $secondary =~ s/^\^\s+(\S+)\s+\^\s*//o;
	  my @secondary = map {TAG::LP::quote($_)} split(/\s+/,$secondary);
	  if (scalar(@secondary) > 1) {
	    $secondary = "${var}::${nodeid}[".join(',',@secondary).']';
	  } elsif (@secondary) {
	    $secondary = "${var}::$secondary[0]";
	  } else {
	    $secondary = $var
	  }
	}
	my $secondlabel = $self->{secondary_label};
	if ($secondlabel) {
	  $secondlabel = TAG::LP::quote($secondlabel);
	} else {
	  $secondlabel = $ref;
	}
	if (exists $self->{secondary_ref}) {
	  my $sref = $self->{secondary_ref};
	  my $var;
	  if ($sref =~ /^\^\s+(\S+)\s+\^\s*/) {
	    $var = TAG::LP::var($1);
	    $sref =~ s/^\^\s+(\S+)\s+\^\s*//o;
	    my @sref = map {TAG::LP::quote($_)} split(/\s+/,$sref);
	    if (scalar(@sref) > 1) {
	      $sref = "${var}::${nodeid}[".join(',',@sref).']';
	    } elsif (@sref) {
	      $sref = "${var}::$sref[0]";
	    } else {
	      $sref = $var
	    }
	    $ref = $sref
	  }
	}
	push(@postprocess,"'\$tagop'($ref,secondary($ref,$secondary,$secondlabel,local))");
      }
    }
##    push(@args, "id= " . &TAG::LP::quote($id) ) if ($id);
    push(@args, "top= " . $top->LP($cat)) if ($top);
    push(@args, "bot= " . $bot->LP($cat)) if ($bot);
    $adjleft and push(@args, "adjleft= $adjleft");
    $adjright and push(@args, "adjright= $adjright");
    $adjwrap and push(@args, "adjwrap= $adjwrap");
    if (($type ne 'lex') && (defined $token) && $token) {
      $token = lex_handle($token,$id);
      push(@args, "token= $token");
    }
    my $args = "";
    if (@args) {
	$args = &TAG::LP::list( \@args,
				{ open => '', 
				  close => "\n" . TAG::LP::prefix($depth) . "at ", 
				  sep => "\n" . TAG::LP::prefix($depth) . "and " }
				);
      }
    my @tmp = ();
    while (@child) {
      my $first = shift @child;
      if (@reducers
	  && !@child
#	  && ref($first) ne 'TAG::Alternative'
	  && (
	      !exists $first->{guards}
	      || !grep {$_->{rel} eq '-' && $_->LP ne 'fail'} @{$first->{guards}}
	     )
	 ) {
	push(@tmp,$first->LP($depth+1,@reducers));
	@reducers=();
      } else {
	push(@tmp,$first->LP($depth+1));
      }
    }
    @child = @tmp;
    ## We have a pb if Node has no child but reducers
    ## that case should not happen !
    # push(@child,'{'.join(', ',map($_->LP($depth+1),@reducers)).'}') if (@reducers);
    if (@child) {
      $child= &TAG::LP::list( \@child,
			      { open => "(\n" . TAG::LP::prefix($depth+1), 
				close => "\n" . TAG::LP::prefix($depth) . ")", 
				sep => ",\n" . TAG::LP::prefix($depth+1) }
			    );
    }
    my $label = $cat;
    if ($type eq 'lex') {
      if (!defined $lex) {  
	## maybe we could use '[${lexset}[]]' 
	## to denote all possible allowed lex ?
	$label = '[_]';
      } elsif ($lex eq '') {
	$label = '[]';
      } else {
	my $xlex = lex_handle($lex,$id);
	$label = "[ $xlex ]";
      }
    }
    ($type eq 'anchor') and $label = "<> $label";
    ($type eq 'coanchor') and $label = "<=> $label";
    ($type eq 'foot') and $label = "* $label";
    ($type eq 'skip') and $label = "'\$skip'";
    my $mask = '';
    if (defined $adj && ($type eq 'anchor' || $type eq 'std' ||$type eq 'coanchor')) {
      $mask = '- ' if ($adj eq 'no');
      $mask = '++ ' if ($adj eq 'strict');
    }
    my $display = "$args$mask$label$child";
    @reducers and push(@postprocess,map($_->LP($depth+1),@reducers));
    if (@postprocess) {
      my $postprocess='';
      $postprocess = '{'.join(' , ',@postprocess).'}';
      if ($type eq 'foot') {
	$display = guard_wrapper($self,$display);
	return "($postprocess, $display)";
      } else {
	return guard_wrapper($self,"($display, $postprocess)");
      }
    } else {
      return guard_wrapper($self,$display);
    }
  }

sub lex_handle {
  my $lex = shift;
  my $id = shift;
  my $varlex='';
  if ($lex =~ /^\^\s+(\S+)\s+\^\s*/) {
    $varlex = TAG::LP::var($1);
    $lex =~ s/^\^\s+(\S+)\s+\^\s*//o;
    $varlex .= '::' if ($id && $lex);
  }
 ## my @lex = split(/\s+/,$lex);
  my @lex = ();
  while ($lex =~ /\G\s*('.+?'|\S+)/og) {
    my $u = $1;
    $u =~ /^'(.+)'$/ and $u = $1;
    push(@lex,$u);
  }
  @lex = map {TAG::LP::quote($_)} @lex;
  if (scalar(@lex) > 1) {
    my $lexset = $map->{lp}{lex_set} ||= 'lex';
     return "${varlex}${lexset}[".join(',',@lex).']';
  } elsif (@lex) {
    return "${varlex}$lex[0]";
  } else {
    return "${varlex}";
  }
}

sub TAG::Reducer::LP {
  my $self = shift;
  return "atom(".$self->{value}[0]->LP.")";
}

sub TAG::Secondary::LP {
  my $self = shift;
  my $out = "'\$tagop'($self->{ref},secondary($self->{ref},$self->{gov},$self->{label},$self->{axis}))";
##  print STDERR "processing secondary $out\n";
  return $out;
}

sub TAG::Not::LP {
  my $self = shift;
  my $kind = shift;
  my @values = map $_->LP($kind,'neg'), @{$self->{'value'}};
  $kind = TAG::LP::lower_or_quote($kind);
  return "$kind\[~ [".join(',',@values)."]\]";
}

sub TAG::VAlt::LP {
  my $self = shift;
  my $kind = shift;
  my $neg = shift || '';
  my @values = map $_->LP($kind), @{$self->{'value'}};
  $kind = TAG::LP::lower_or_quote($kind);
  my $res = join(',',@values);
  $res = "$kind\[$res\]" unless ($neg);
  return $res;
}


sub TAG::Var::LP {
  my $self = shift;
  my $kind = shift;
  my $name = TAG::LP::var($self->{name});
  my @values = map $_->LP($kind), @{$self->{'value'}};
  $kind = TAG::LP::lower_or_quote($kind);
  my $output = $name;
  return $output unless @values;
  $output .= ' :: ';
  if (@values > 1 
      ## following line not necessary, except to mark
      ## single values from finite set domains
      ## || (defined $fset{$kind} && !isa($self->{value}[0],"TAG::Feature"))
     ) {
    $output .= "$kind\[".join(',',@values)."\]";
  } else {
    $output .= $values[0];
  }
  return $output;
}

sub TAG::Or::LP {
  my $self =shift;
  my @guards = @{$self->{'guards'}};
  return 'fail' unless (@guards);
  return $guards[0]->LP if (@guards == 1);
  return '( '.join( ' ; ', map($_->LP,@guards)).' )';
}

sub TAG::And::LP {
  my $self =shift;
  my @guards = @{$self->{'guards'}};
  return 'fail' unless (@guards);
  return $guards[0]->LP if (@guards == 1);
  return '( '.join( ' , ', map($_->LP,@guards)).' )';
}


sub TAG::NGuards::LP {
  my $self = shift;
  my @guards = @{$self->{'guards'}};
  return @guards ? ($self->{rel} => join(',',map($_->LP,@guards))) : ();
}

sub TAG::NGuard::LP {
  my $self = shift;
  my $kind = $self->{type} || "none";
  $kind = $map->{fs_redirect}{$kind} if (defined $map->{fs_redirect}{$kind});
  return join( ' = ', map($_->LP($kind),@{$self->{'values'}}) );
}

sub TAG::NXGuard::LP {
  my $self = shift;
  my $kind = $self->{type} || "none";
  $kind = $map->{fs_redirect}{$kind} if (defined $map->{fs_redirect}{$kind});
  my $ret = join( ' = ', map($_->LP($kind),@{$self->{'values'}}) );
##  return "($ret xor true)";
  return "(\\+ \\+ $ret )";
}

sub TAG::Lemma::LP {
    my $self = shift;
    my $name = TAG::LP::quote($self->{'name'});
    my $cat = TAG::LP::lower_or_quote($self->{'cat'});
    my @anchors = map $_->LP(), @{ $self->{'anchors'} };
    my $anchors = &TAG::LP::single_or_list(\@anchors,
					   { open => "[ ",
					     close => "\n\t]",
					     sep => ",\n\t  "
					     });
    return "tag_lemma($name,$cat,\n\t$anchors\n\t).";
}

sub TAG::Lexicon::LP {
    my $self = shift;
    my $lex = TAG::LP::quote($self->{'lex'});
    return map( $_->LP($lex), @{$self->{'lemma'}} );
}

sub TAG::LemmaRef::LP {
    my ($self,$lex) = @_;
    my $name = TAG::LP::quote($self->{'name'});
    my $cat = TAG::LP::lower_or_quote($self->{'cat'});
    my $feature = $self->{'feature'};
    if ($feature) {
	$feature = $feature->LP($cat);
    } else {
	$feature = '_';
    }
    return "tag_lexicon($lex," . $name . ", $cat, $feature).";
}

sub TAG::Feature::LP {
    my $self = shift;
    my $kind = $self->{'type'};
    my $qkind;
    if ($kind) {
      $qkind = TAG::LP::quote($kind);
    } else {
      $kind = shift;
      $qkind = $kind;		# assume it is quoted
    }
    my @features = map $self->{'f'}{$_}->LP, sort (keys %{$self->{'f'}});
    if (@features 
	|| defined $features{$kind} 
	|| defined $features{$qkind}
       ) {
	return "$qkind"."{" . join(', ',@features) . "}";
    } else {
	return "$qkind";
    }
}

sub TAG::Comp::LP {
  my $self = shift;
  my $name = $self->{'name'};
  my $kind = (defined $map->{fs_redirect}{$name}) ? $map->{fs_redirect}{$name} : $name;
  my @values = map $_->LP($kind), @{$self->{'value'}};
  $kind = TAG::LP::lower_or_quote($kind);
  $name = TAG::LP::lower_or_quote($name);
  return unless (@values);
  my $output = "$name => ";
  if (@values > 1 
      ## following line not necessary, except to mark
      ## single values from finite set domains
      ## || (defined $fset{$kind} && !isa($self->{value}[0],"TAG::Feature"))
     ) {
    $output .= "$kind\[".join(',',@values)."\]";
  } else {
    $output .= $values[0];
  } 
  return $output;
}

sub TAG::Narg::LP {
    my $self = shift;
    my $cat = shift;
    my $name = $self->{'name'};
    my $kind = (defined $map->{fs_redirect}{$cat}) ? $map->{fs_redirect}{$cat} : $cat;
    my @values = map $_->LP($kind), @{$self->{'value'}};
    $kind = TAG::LP::lower_or_quote($kind);
    $name = TAG::LP::lower_or_quote($name);
    return unless (@values);
    my $output = "";
    if (@values > 1 
	## following line not necessary, except to mark
	## single values from finite set domains
	## || (defined $fset{$kind} && !isa($self->{value}[0],"TAG::Feature"))
       ) {
      $output .= "$kind\[".join(',',@values)."\]";
    } else {
      $output .= $values[0];
    } 
    return $output;
  }


sub TAG::Equation::LP {
  my $self = shift;
  my $type = &TAG::LP::quote($self->{'type'});
  my $node_id = $self->{'node_id'};
  my $fs = $self->{'feature'};
  my ($node_cat) = ($node_id =~ /^(\w+)_/);
  $node_cat = TAG::LP::lower_or_quote($node_cat);
  $node_id = TAG::LP::quote($node_id);
  return "$type = " . $fs->LP($node_cat) . " at $node_id";
}

sub TAG::Anchor::LP {
    my $self = shift;
    my @args = ();
    my @families = map &TAG::LP::quote($_), @{ $self->{'family'} };
    push(@args,
	 "name => " . &TAG::LP::single_or_list(\@families,
					       { open => '[ ',
						 close => ' ]',
						 sep => ', '
						 }));
    my @coanchors = map $_->LP(), @{ $self->{'coanchors'} };
    if (@coanchors) {
	push(@args,
	     "coanchors => " . &TAG::LP::list(\@coanchors,
					      { open => '[ ',
						close => ' ]',
						sep => ', '
						}));
    }
    my @equations = map $_->LP(), @{ $self->{'equations'} };
    if (@equations) {
	push(@args,
	     "equations => " . &TAG::LP::list(\@equations,
					      { open => '[ ',
						close => ' ]',
						sep => ",\n\t\t\t\t       "
						}));
    }
    return "tag_anchor" . &TAG::LP::list( \@args,
					  { open => "{\t",
					    close => ' }',
					    sep => ",\n\t\t\t"
					    }
					 );
}

sub TAG::Coanchor::LP {
    my $self = shift;
    my $node_id = TAG::LP::quote($self->{'node_id'});
    my @lex = map $_->LP(), @{$self->{'lex'} };
    my $lex = &TAG::LP::single_or_list(\@lex,
				       { open => '(',
					 close => ')',
					 sep => ';' 
					 });
    return "$node_id=$lex";
}

sub TAG::Val::LP {
    my $self = shift;
    return TAG::LP::quote( $self->{'text'} );
}

sub TAG::Lex::LP {
    my $self = shift;
    return TAG::LP::quote( $self->{'text'} );
}

sub TAG::Plus::LP {
    my $self = shift;
    return "(+)" ;
}

sub TAG::Minus::LP {
    my $self = shift;
    return "(-)" ;
}

1;

__END__

=head1 NAME

TAG::LP - output content of a Tree Adjoining Grammar (TAG) as a string
in Logic Programming format.

=head1 SYNOPSIS

 use TAG::RCG;
 
 # using LP method on a TAG::Document 
 print $tag_object->LP();

=head1 DESCRIPTION

Calling `C<LP>' on an TAG object returns it as string in Logic
Programming format. It may then be compiled using system 'C<DyALog>'.

=head1 OPTIONS

    none.

=over 4

=back

=head1 EXAMPLES

Here is an example of parsing a TAG in XML format and emitting it as a LP:

    use TAG::XML::Parser;
    use TAG::LP;

    my $parser = TAG::XML::Parser->new ();
    my $doc = $parser->parsefile( $grammarfile );

    print $doc->LP();

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

DyALog <http://alpage.inria.fr/~clerger>

=cut
