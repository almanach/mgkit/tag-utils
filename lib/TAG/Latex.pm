# $Id$
package TAG::Latex;

require 5.005;
use strict;
use warnings;
use TAG;

sub TAG::Document::latex {
    my $self = shift;
    print <<'EOF';
\documentclass[dvips,twosides]{report}

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\usepackage{latexsym}
\usepackage[leqno,centertags]{amsmath}

\usepackage{pstricks}
\usepackage{pst-node}
\usepackage{pst-tree}

\psset{arrows=->,unit=.75cm,treesep=.75cm,levelsep=2cm,nodesep=1pt}

\makeatletter
%%\def\lab#1{~[tnpos=r]{#1}}
\def\lab#1{{#1}}
\def\stdN{\@ifnextchar-{\@stdN}{\@stdN{}}}
\def\anchorN{\@ifnextchar-{\@anchorN}{\@anchorN{}}}
\def\footN{\@ifnextchar[{\@footN}{\@footN[]}}
\def\lexN#1{\TR{[#1]}}
\def\substN{\@ifnextchar[{\@substN}{\@substN[]}}
%%
\def\@stdN#1{\@ifnextchar[{\@@stdN{#1}}{\@@stdN{#1}[]}}
\def\@@stdN#1[#2]#3#4{\pstree{\TR{#3}~[tnpos=r,tnyref=-.6]{\lab{#2}}~[tnpos=l]{#1}}{#4}}
\def\@anchorN#1{\@ifnextchar[{\@@anchorN{#1}}{\@@anchorN{#1}[]}}
\def\@@anchorN#1[#2]#3{\TR{<>#3}~[tnpos=r,tnyref=-.6]{\lab{#2}}~[tnpos=l]{#1}}
\def\@footN[#1]#2{\TR{#2}~[tnpos=r]{$\negthickspace\star$}~[tnpos=r,tnyref=-.6]{\lab{#1}}}
\def\@substN[#1]#2{\TR{#2}~[tnpos=r]{$\negthickspace\downarrow$}~[tnpos=r,tnyref=-.6]{\lab{#1}}}
\makeatother

%\twocolumn

\begin{document}

EOF

    ## Morph
    
    print <<EOF ;
\\chapter{Lexicon}

    \\begin{description}

EOF
 
    ($self->{'lexicon'}{$_})->latex() foreach (sort (keys %{$self->{'lexicon'}}));

    ## Lemma
    print <<EOF;

    \\end{description}

\\chapter{Lemma}

    \\begin{description}

EOF

    foreach my $lemma (sort (keys %{ $self->{'lemma'} })) {
        my $tmp = $self->{'lemma'}{$lemma};
        $tmp->{$_}->latex() foreach (sort (keys %{ $tmp }));
    }

    ## Families
    print <<EOF;

    \\end{description}

\\chapter{Families}

    \\begin{description}

EOF

        ($self->{'families'}{$_})->latex() foreach (sort (keys %{$self->{'families'}}));


    ## Trees
    print <<EOF;

    \\end{description}

\\chapter{Trees}

    \\begin{description}

EOF

    ($self->{'trees'}{$_})->latex() foreach (sort (keys %{$self->{'trees'}}));

    print <<EOF;

    \\end{description}

\\end{document}

EOF

}

sub iso {
    my $s = shift;
    $s =~ tr/\0-\x{ff}//UC;
    $s =~ s/_/\\_/og;
    return $s;
}


sub TAG::Lexicon::latex {
    my $self = shift;
    my $lex = &iso($self->{'lex'});
    print <<EOF;
\\item [ $lex ] \\relax
EOF
    $_->latex() foreach @{$self->{lemma}};
}

sub TAG::LemmaRef::latex {
    my $self = shift;
    my $name = &iso($self->{'name'});
    my $cat = &iso($self->{'cat'});
    print <<EOF
	$name/$cat \\\\
EOF
}

sub TAG::Lemma::latex {
    my $self = shift;
    my $name = &iso($self->{'name'});
    my $cat = &iso($self->{'cat'});
    print <<EOF;
\\item [  $name/$cat ] \\relax
EOF
}


sub TAG::Family::latex {
    my $self = shift;
    my $name = &iso($self->{'name'});
    print "\n\\item [$name] ", join('; ',
				 map(&iso($_),@{$self->{'trees'}}));
}

sub TAG::Tree::latex {
    my $self = shift;
    my $name = &iso($self->{'name'});
    my $type = &iso($self->{'type'});
    print "\n\\item [  $name ] ($type) ", join('; ',
					     map(&iso($_),
						 @{$self->{'family'}}));
    print "\n\n";
    $self->{'root'}->latex();
    print "\n\n";
}


sub TAG::Node::latex {
    my $self = shift;
    my $type = "$self->{'type'}";
    my $id = &iso($self->{'id'});
    $id = $id ? "[$id]" : '';
    my $adj = ($self->{'adj'} eq 'no') ? '-' : '';
    if ("$type" eq 'subst') {
	print '\substN',$id,"{$self->{cat}}";
    } elsif ("$type" eq 'anchor') {
	print '\anchorN',$adj,$id,"{$self->{cat}}";
    } elsif ("$type" eq 'lex') {
	print '\lexN',$id,"{$self->{lex}}";
    } elsif ("$type" eq 'foot') {
	print '\footN',$id,"{$self->{cat}}";
    } else {
	print '\stdN',$adj,$id,"{$self->{cat}}{";
	$_->latex() foreach (@{$self->{'child'}});
	print '}';
    }
}


1;

__END__

=head1 NAME

TAG::Latex - output content of a Tree Adjoining Grammar (TAG) in Latex
format (using pstricks)

=head1 SYNOPSIS

 use TAG::Latex;
 
 # using LP method on a TAG::Document 
 print $tag_object->latex();

=head1 DESCRIPTION

Calling `C<latex>' on an TAG object returns a latex version of it.

=head1 OPTIONS

    none.

=over 4

=back

=head1 EXAMPLES

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)
