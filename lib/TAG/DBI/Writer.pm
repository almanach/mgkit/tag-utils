# $Id$
package TAG::DBI::Writer;

require 5.005;
use strict;
use warnings;
use TAG;
use DBI;
#use bytes;

our %data =  ( 
	       'models' => { 
		   'families' => [ qw{name tree} ] ,
		   'trees' => [ qw{name type root} ],
		   'nodes' => [ qw{ref tree parent index id cat lex type adj top bot}],
		   'morph' => [ qw{lex}],
		   'lemmaref' => [ qw{lex name cat feature} ],
		   'lemma' => [ qw{ref name cat} ],
		   'anchors' => [ qw{ref lemma} ],
		   'anchors_family' => [ qw{ref family} ],
		   'coanchors' => [ qw{ref anchor node_id} ],
		   'coanchors_lex' => [ qw{ref lex} ],
		   'equations' => [ qw{anchor node_id type fs} ],
		   'fs' => [ qw{ref name type val} ]
		   }
	       );

sub quote {
    my $s = shift;
    $s =~ s/\'/\'\'/og;
#    $s =~ tr/\0-\x{ff}//UC;
    return $s;
};

sub prepare {
    my $table = shift;
    my $model = $data{'models'}{$table};
    my $values = join(',',('?') x @{ $model });
    my $fields = join(',',@{ $model });
    $data{'stm'}{$table} = $data{'dbh'}->prepare("insert into $table ($fields) values ($values)");
}

sub init {
    my $doc = shift;
    my $dbh = DBI->connect(@_) || die "can't connect to database";
    @data{'doc','dbh','ref'} = ( $doc,
				 $dbh,
				 {   'node' => 0,
				     'lemmaref' => 0,
				     'anchor' => 0,
				     'coanchor' => 0,
				     'equation' => 0,
				     'lemma' => 0,
				     'feature' => 0
				     },
				 );
    &prepare($_) foreach (keys %{ $data{'models'} });
    $dbh->do( <<INIT );
    delete from families;
    delete from trees;
    delete from nodes;
    delete from anchors;
    delete from anchors_family;
    delete from coanchors;
    delete from coanchors_lex;
    delete from equations;
    delete from morph;
    delete from lemma;
    delete from lemmaref;
    delete from fs;
INIT
}

sub finish {
    $data{'dbh'}->disconnect;
}

sub TAG::Document::dbi_dump {
    my $self = shift;
    &init($self,@_);
    $_->dbi_dump foreach (values %{$self->{'families'}});
    $_->dbi_dump foreach (values %{$self->{'lexicon'}});
    foreach my $lemma (values %{$self->{'lemma'}}){
	$_->dbi_dump foreach (values %$lemma);
    }
    &finish;
}

sub TAG::Family::dbi_dump {
    my $self = shift;
    $data{'stm'}{'families'}->execute($self->{'name'},$_) foreach (@{ $self->{'trees'} });
    $data{'doc'}{'trees'}{$_}->dbi_dump foreach (@{ $self->{'trees'} });
}

sub TAG::Tree::dbi_dump {
    my $self = shift;
    my $root = $self->{'root'}->dbi_dump($self->{'name'},-1,-1);
    $data{'stm'}{'trees'}->execute($self->{'name'},$self->{'type'},$root);
}

sub TAG::Node::dbi_dump {
    my $self = shift;
    my $tree = shift;
    my $parent = shift;
    my $index = shift;
    my $ref = $data{'ref'}{'node'}++;
    my $i=1;
    my @child = map( $_->dbi_dump($tree,$ref,$i++), @{ $self->{'child'} } );
    my $top = $self->{'top'};
    my $bot = $self->{'bot'};
    $top = $top ? $top->dbi_dump : -1;
    $bot = $bot ? $bot->dbi_dump : -1;
    $data{'stm'}{'nodes'}->execute($ref,
				   $tree,
				   $parent,
				   $index,
				   defined $self->{'id'}  ? "$self->{'id'}"  : "",
				   defined $self->{'cat'} ? "$self->{'cat'}" : "",
				   defined $self->{'lex'} ? "$self->{'lex'}" : "",
				   "$self->{'type'}",
				   defined $self->{'adj'} ? "$self->{'adj'}" : "",
				   $top,
				   $bot);
    return $ref;
}

sub TAG::Lexicon::dbi_dump {
    my $self = shift;
    my $lex = &quote($self->{'lex'});
    my @lemmaref = map( $_->dbi_dump($lex), @{ $self->{'lemma'} } );
    $data{'stm'}{'morph'}->execute($lex);
}

sub TAG::LemmaRef::dbi_dump {
    my $self = shift;
    my $lex = shift;
    my $fs = $self->{'feature'};
    $fs = $fs ? $fs = $fs->dbi_dump : -1;
    $data{'stm'}{'lemmaref'}->execute($lex,
				      &quote($self->{'name'}),
				      $self->{'cat'},
				      $fs);
}

sub TAG::Lemma::dbi_dump {
    my $self = shift;
    my $ref = $data{'ref'}{'lemma'}++;
    $data{'stm'}{'lemma'}->execute($ref,
				   &quote($self->{'name'}),
				   $self->{'cat'});
    $_->dbi_dump($ref) foreach (@{ $self->{'anchors'} });
}

sub TAG::Anchor::dbi_dump {
    my $self = shift;
    my $lemma = shift;
    my $ref = $data{'ref'}{'anchor'}++;
    $data{'stm'}{'anchors'}->execute($ref,$lemma);
    $data{'stm'}{'anchors_family'}->execute($ref,$_) foreach (@{ $self->{'family'}});
    $_->dbi_dump($ref) foreach (@{ $self->{'coanchors'} } );
    $_->dbi_dump($ref) foreach (@{ $self->{'equations'} } );
}

sub TAG::Coanchor::dbi_dump {
    my $self = shift;
    my $anchor =shift;
    my $ref = $data{'ref'}{'coanchor'}++;
    my @lex = map $_->{'text'}, @{ $self->{'lex'} };
    $data{'stm'}{'coanchors'}->execute($ref,$anchor,$self->{'node_id'});
    $data{'stm'}{'coanchors_lex'}->execute($ref,&quote($_)) foreach (@lex);
}

sub TAG::Equation::dbi_dump {
    my $self = shift;
    my $anchor = shift;
    my $fs = $self->{'feature'};
    $fs = $fs ? $fs->dbi_dump : -1;
    $data{'stm'}{'equations'}->execute($anchor,
				       $self->{'node_id'},
				       $self->{'type'},
				       $fs);
}

sub TAG::Feature::dbi_dump {
    my $self = shift;
    my $ref = $data{'ref'}{'feature'}++;
    $_->dbi_dump($ref) foreach (values %{ $self->{'f'} });
    return $ref;
}

sub TAG::Comp::dbi_dump {
    my $self = shift;
    my $fs = shift;
    my $f = $self->{'name'};
    $data{'stm'}{'fs'}->execute($fs,$f,'link',$self->{'id'}) if (defined $self->{'id'});
    $_->dbi_dump($f,$fs) foreach (@{ $self->{'value'} });
}

sub TAG::Val::dbi_dump {
    my $self = shift;
    my $f = shift;
    my $fs = shift;
    $data{'stm'}{'fs'}->execute($fs,$f,'val',$self->{'text'});
}

sub TAG::Link::dbi_dump {
    my $self = shift;
    my $f = shift;
    my $fs = shift;
    $data{'stm'}{'fs'}->execute($fs,$f,'link',$self->{'id'});
}

sub TAG::Plus::dbi_dump {
    my $self = shift;
    my $f = shift;
    my $fs = shift;
    $data{'stm'}{'fs'}->execute($fs,$f,'plus','');
}

sub TAG::Minus::dbi_dump {
    my $self = shift;
    my $f = shift;
    my $fs = shift;
    $data{'stm'}{'fs'}->execute($fs,$f,'minus','');
}


1;

__END__

=head1 NAME

TAG::DBI - Dump/restore TAG objects into/from databases

=head1 SYNOPSIS

 use TAG::DBI::Writer;

=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
