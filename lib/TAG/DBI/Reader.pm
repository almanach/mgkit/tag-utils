# $Id$
package TAG::DBI::Reader;

require 5.005;
use strict;
use warnings;
use TAG;
use DBI;

our %data =  ( 
	       'models' => { 
		   'families'       => 'select distinct name from families' ,
		   'families_tree'  => 'select tree from families where name=?',
		   'trees'          => 'select * from trees where name=?',
		   'nodes'          => 'select * from nodes where ref= int ?',
		   'nodes_son'      => 'select * from nodes where parent= int ? order by index',
		   'morph'          => 'select lex from morph',
		   'lemmaref'       => 'select * from lemmaref where lex=?',
		   'lemma'          => 'select * from lemma',
		   'anchors'        => 'select * from anchors where lemma = int ?',
		   'anchors_family' => 'select * from anchors_family where ref= int ?',
		   'coanchors'      => 'select ref, node_id from coanchors where anchor= int ?',
		   'coanchors_lex'  => 'select lex from coanchors_lex where ref= int ?',
		   'equations'      => 'select node_id, type, fs from equations where anchor= int ?',
		   'fs_name'        => 'select distinct name from fs where ref= int ?',
		   'fs_val'         => 'select type, val from fs where ref= int ? and name=?'
		   }
	       );


sub translate {
    my $s = shift;
#    $s =~ tr/\0-\0xff//CU;
    return $s;
};

sub quote {
    my $s = shift;
    $s =~ s/\'/\'\'/og;
#    $s =~ tr/\0-\x{FF}//UC;
    return $s;
};

sub prepare {
    my $table = shift;
    my $model = $data{'models'}{$table};
#    print STDERR "Preparing model for $table: $model\n";
    $data{'stm'}{$table} = $data{'dbh'}->prepare($model);
    my $sth = $data{'stm'}{$table};
#    print STDERR "INFO @{$sth->{NAME_lc}}\n";
#    print STDERR "INFO fields=$sth->{NUM_OF_FIELDS}\n";
#    print STDERR "INFO params=$sth->{NUM_OF_PARAMS}\n";
}

sub init {
    my $doc = shift;
    my $dbh = DBI->connect(@_) || die "can't connect to database";
    @data{'doc','dbh'} = ( $doc,$dbh);
    &prepare($_) foreach (keys %{ $data{'models'} });
}

sub finish {
    $data{'dbh'}->disconnect;
}

sub TAG::Document::dbi_read {
    my $self = shift;
    my $entry;
    &init($self,@_);
    my $stm = $data{'stm'}{'families'};
    $stm->execute;
    while ( $entry = $stm->fetchrow_hashref ) {
	my $name = $entry->{'name'};
	$self->{'families'}{$name} = TAG::Family->dbi_read($name);
    }
    $stm->finish;
    $_->dbi_read foreach (values %{ $self->{'trees'} });
    $stm = $data{'stm'}{'morph'};
    $stm->execute;
    while ( $entry = $stm->fetchrow_hashref ) {
	my $lex = &translate($entry->{'lex'});
	$self->{'lexicon'}{$lex} = TAG::Lexicon->dbi_read($lex);
    }
    $stm->finish;
    $stm = $data{'stm'}{'lemma'};
    $stm->execute;
    while ( $entry = $stm->fetchrow_hashref ) {
	my $name = &translate($entry->{'name'});
	$self->{'lemma'}{$name}{$entry->{'cat'}} = 
	  TAG::Lemma->dbi_read($name,
			       $entry->{'cat'},
			       $entry->{'ref'}
			       );
    }
    $stm->finish;
    &finish;
}

sub TAG::Family::dbi_read {
    my $self = shift->new( 'trees' => [] );
    $self->{'name'} = shift;
    my $stm = $data{'stm'}{'families_tree'};
    my $entry;
    $stm->execute($self->{'name'});
    while ( $entry = $stm->fetchrow_hashref ) {
	my $name = $entry->{'tree'};
	my $tree = $data{'doc'}{'trees'}{$name} || TAG::Tree->new( 'name' => $name, 
								   'families' => [] );
	push @{ $tree->{'families'} }, $self->{'name'};
	push @{ $self->{'trees' } }, $name;
	$data{'doc'}{'trees'}{$name} = $tree;
    }
    $stm->finish;
    return $self;
}

sub TAG::Tree::dbi_read {
    my $self = shift;
    my $stm = $data{'stm'}{'trees'};
##    print STDERR "Tree $self->{name}\n";
    $stm->execute($self->{'name'});
    my $entry = $stm->fetchrow_hashref;
    if ($entry) {
	$self->{'type'} = $entry->{'type'};
	my $root= $entry->{'root'};
	my $stm2 = $data{'stm'}{'nodes'};
	$stm2->execute($root);
	my $entry2 = $stm2->fetchrow_hashref;
	$self->{'root'} = TAG::Node->dbi_read($entry2);
	$stm2->finish;
    }
    $stm->finish;
    return $self;
}

our @node_attr = ('id','cat','type','adj','lex');

sub TAG::Node::dbi_read {
    my $self = shift->new( child => [] );
    my $entry = shift;
    my $ref = $entry->{'ref'};
    foreach my $attr (@node_attr) {
	$self->{$attr} = $entry->{$attr} if (defined $entry->{$attr});
    }
    if ($entry->{'top'} ge 0) {
	$self->{'top'} = TAG::Feature->dbi_read($entry->{'top'});
    }
    if ($entry->{'bot'} ge 0) {
	$self->{'bot'} = TAG::Feature->dbi_read($entry->{'bot'});
    }
    my $stm = $data{'stm'}{'nodes_son'};
    $stm->execute($ref);
    my @child=();
    push(@child, $entry) while ($entry = $stm->fetchrow_hashref);
    $stm->finish;
    $self->{'child'} = [ map TAG::Node->dbi_read($_), @child ];
    return $self;
}

sub TAG::Lexicon::dbi_read {
    my $self = shift->new( 'lemma' => [] );
    $self->{'lex'} = shift;
    my $stm = $data{'stm'}{'lemmaref'};
    my $entry;
    $stm->execute(&quote($self->{'lex'}));
    while ($entry = $stm->fetchrow_hashref) {
	push @{$self->{'lemma'}}, TAG::LemmaRef->dbi_read( &translate($entry->{'name'}),
							   $entry->{'cat'});
    }
    $stm->finish;
    return $self;
}

sub TAG::LemmaRef::dbi_read {
    my $self = shift->new;
    $self->{'name'} = shift;
    $self->{'cat'} = shift;
    return $self;
}

sub TAG::Lemma::dbi_read {
    my $self=shift->new( anchors=>[] );
    $self->{'name'} = shift;
    $self->{'cat'} = shift;
    my $ref = shift;
    my $stm = $data{'stm'}{'anchors'};
    $stm->execute($ref);
    my $entry;
    while ($entry = $stm->fetchrow_hashref) {
  	push @{$self->{'anchors'}}, TAG::Anchor->dbi_read($entry->{'ref'});
    }
    $stm->finish;
    return $self;
}

sub TAG::Anchor::dbi_read {
    my $self=shift->new( 'family' => [], 
			 'coanchors' => [],
			 'equations' => []
			 );
    my $ref = shift;
    my $stm = $data{'stm'}{'anchors_family'};
    $stm->execute($ref);
    my $entry;
    while ($entry = $stm->fetchrow_hashref) {
	push @{$self->{'family'}}, $entry->{'family'};
    }
    $stm->finish;
    $stm = $data{'stm'}{'coanchors'};
    $stm->execute($ref);
    while ($entry = $stm->fetchrow_hashref) {
	push @{$self->{'coanchors'}}, TAG::Coanchor->dbi_read($entry->{'node_id'},
							      $entry->{'ref'});
    }
    $stm->finish;
    $stm = $data{'stm'}{'equations'};
    $stm->execute($ref);
    while ($entry = $stm->fetchrow_hashref) {
	push @{$self->{'equations'}}, TAG::Equation->dbi_read($entry->{'node_id'},
							      $entry->{'type'},
							      $entry->{'fs'});
    }
    $stm->finish;
    return $self;
}

sub TAG::Coanchor::dbi_read {
    my $self=shift->new( 'lex' => [] );
    $self->{'node_id'} = shift;
    my $ref = shift;
    my $stm = $data{'stm'}{'coanchors_lex'};
    $stm->execute($ref);
    my $entry;
    while ($entry = $stm->fetchrow_hashref) {
	push @{$self->{'lex'}}, TAG::Lex->new('text'=>&translate($entry->{'lex'}));
    }
    $stm->finish;
    return $self;
}

sub TAG::Equation::dbi_read {
    my $self=shift->new;
    $self->{'node_id'} = shift;
    $self->{'type'} = shift;
    $self->{'feature'} = TAG::Feature->dbi_read(@_);
    return $self;
}

sub TAG::Feature::dbi_read {
    my $self=shift->new;
    my $ref = shift;
    my $stm = $data{'stm'}{'fs_name'};
    $stm->execute($ref);
    my $entry;
    while ($entry = $stm->fetchrow_hashref) {
	$self->{'f'}{$entry->{'name'}} = TAG::Comp->dbi_read( $entry->{'name'},
							      $ref );
    }
    $stm->finish;
    return $self;    
}

sub TAG::Comp::dbi_read {
    my $self=shift->new( 'value' => [] );
    my $name = shift;
    my $ref = shift;
    $self->{'name'}=$name;
    my $stm = $data{'stm'}{'fs_val'};
    $stm->execute($ref,$name);
    my $entry;
    while ($entry = $stm->fetchrow_hashref) {
	my $type = $entry->{'type'};
	push(@{ $self->{'value'} },TAG::Plus->new), next if ($type eq 'plus');
	push(@{ $self->{'value'} },TAG::Minus->new), next if ($type eq 'minus');
	push(@{ $self->{'value'} },TAG::Val->new('text'=>$entry->{'val'})), next if ($type eq 'val');
	$self->{'id'} = $entry->{'val'}, next if ($type eq 'link');
    }
    $stm->finish;
    return $self;    
}


1;

__END__

=head1 NAME

TAG::DBI::Reader - Restore TAG objects from databases

=head1 SYNOPSIS

 use TAG::DBI::Reader;

=head1 DESCRIPTION

This module provides ways to build TAG objects from the contents of a
database.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)
TAG::DBI::Writer (3)
DBI (3)

=cut
