# $Id$
package TAG::Tools;

use 5.005;
use strict;
use warnings;

use Exporter;
our @ISA  = qw(Exporter);
our @EXPORT   = qw/&UTF8toISO &ISOtoUTF8/;

sub UTF8toISO {
    return pack("C*",unpack("U*",shift));
}

sub ISOtoUTF8 {
    return pack("U*",unpack("C*",shift));
}

1;

__END__

=head1 NAME

TAG::Tools

=head1 SYNOPSIS

 use TAG::Tools;
    
=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
