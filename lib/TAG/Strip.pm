# $Id$
package TAG::Strip;

require 5.005;
use strict;
use warnings;
use TAG;

sub TAG::Document::strip {
    my $self=shift;
    $_->strip() foreach (values %{$self->{'lexicon'}});
    $_->strip() foreach (values %{$self->{'trees'}});
    foreach my $lemma (values %{$self->{'lemma'}}){
	$_->strip() foreach (values %{$lemma});
    }
}


sub TAG::Lexicon::strip {
    my $self=shift;
    $_->strip() foreach (@{$self->{'lemma'}});
}

sub TAG::LemmaRef::strip {
    my $self=shift;
    delete $self->{'feature'};
}

sub TAG::Lemma::strip {
    my $self=shift;
    $_->strip() foreach (@{$self->{'anchors'}});
}

sub TAG::Anchor::strip() {
    my $self=shift;
    $self->{'coanchors'}=[];
    $self->{'equations'}=[];
}

sub TAG::Tree::strip() {
    my $self=shift;
    $self->{'root'}->strip();
}

sub TAG::Node::strip() {
    my $self=shift;
    delete $self->{'top'};
    delete $self->{'bot'};
    $_->strip() foreach (@{$self->{'child'}});
}

1;

__END__

=head1 NAME

TAG::Strip - strip Feature Structure information from a Tree
Adjoining Grammar.

=head1 SYNOPSIS

 use TAG::Strip;

 # using LP method on a TAG::Document
 print $tag_object->strip();

=head1 DESCRIPTION

Calling `C<strip>' on an TAG object strips it and its descendants
from all information relative to Feature Structures.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut 
