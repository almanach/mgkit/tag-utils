# $Id$
package TAG::Header;

require 5.005;
use strict;
use warnings;
use Data::Grove;
use TAG;
##use bytes;

our @ISA         = qw(Data::Grove);

our $header;
our $map;

sub lp {
    my $string = shift;
    $string = "" unless (defined $string);
    return quote($string) if (defined $map->{lp}{no_normalize});
    $string =~ tr/-/_/;
    return lc($string);
}

sub quote {
    my $string = shift;
    $string = "" unless (defined $string);
##    return $string if ($string =~ /^[a-z_]\w*$/o);
    return $string if ($string =~ /^[a-z]\w*$/o);
    return $string if ($string =~ /^\d+$/);
    return $string if ($string =~ /^[+^<>=~?\#\@\$&\`-]$/);
    $string =~ s/\'/\'\'/;
    return "\'$string\'";
}

sub print_as_lp {
    my $self = shift;
    print <<EOF;

:-require 'tag_generic.pl'.

%%% Features attached to each category

EOF
    foreach my $cat (sort (keys %{$self->{'cat'}})) {
	my @values = map( lp($_),
			  sort(
			       grep {$self->{cat}{$cat}{$_} eq 'yes'}
			       keys %{$self->{'cat'}{$cat}}));
	if (my $order = $map->{fs_order}{$cat}) {
	  my @order = split(/\s+/,$order);
	  my $pos = 1;
	  my %order = map {$_ => $pos++} @order;
	  @values = sort { ($order{$a} || 100) <=> ($order{$b} || 101) } @values;
	}
	$cat = lp($cat);
	print ":-features($cat,[",join(',', @values),"]).\n";
    }
    print "\n\n";
    print <<EOF;
%%% Default Feature Structure attached to each category

EOF
    print <<EOF;
%%% Definition of finite sets

EOF
    foreach my $f (sort (keys %{$self->{'f'}})) {
	my @values = map(quote($_),
			 sort(grep {$_ ne '-'} keys %{$self->{'f'}{$f}}));
	#  to ensure that (-) is always in first position in finite set
	#  help for better indexng at parsing time
	if (exists $self->{'f'}{$f}{"-"}) {
	  unshift(@values,quote("-"));
	}
	my $prefix = $self->{'finite_set'}{$f} ? "" : "%% ";
	$f = lp($f);
	print "$prefix:-finite_set($f,[",join(',', @values),"]).\n";
    }
    print "\n\n";
    foreach my $cat (sort (keys %{$self->{'cat'}})) {
      $cat = lp($cat);
      my $features = join(', ',
			  map( "$_ => ${_}[]",
			       sort(
				    grep {$self->{cat}{$cat}{$_} eq 'yes'
					    && exists $self->{fs_fill}{$_}
					    && exists $self->{finite_set}{$_}
					    && !exists $self->{cat}{$_}
					    && !exists $self->{fs_open}{$_}
					  }
				    keys %{$self->{'cat'}{$cat}})));
      print ":-tag_features($cat,$cat\{$features\},$cat\{$features\}).\n";
    }
    print "\n\n";
    print <<EOF;
%%% Models of anchor constraints

EOF
    foreach my $family (sort (keys %{ $self->{'anchors'} })) {
	my $entry = $self->{'anchors'}{$family};
	my @coanchors = sort (keys %{ $entry->{'coanchor'} });
	my @equations = ();
	my @all=("name => ".quote($family));
	push(@all,"coanchors => [\t".join(', ',map(quote($_),@coanchors))." ]") if (@coanchors);
	foreach my $node (sort (keys %{ $entry->{'equation'} })) {
	    my ($cat) = ($node =~ /^(\w+?)_/);
	    $cat = lp($cat);
	    my @vars = ();
	    my @eq = ();
	    foreach my $type (keys %{ $entry->{'equation'}{$node} }){
		my @f = map( lp(lc($_)),
			     keys %{ $entry->{'equation'}{$node}{$type} });
		foreach my $var (@f){
		    push(@vars,$var) unless grep($_ eq $var,@vars);
		}
		@f = map("$_ => " . uc("${node}_$_"),@f);
		push(@eq,
		     "$type=$cat\{ " . join(', ',@f)  . " }");
	    }
	    my $vars = join(',',map(uc("${node}_$_"),sort @vars)) ;
	    push(@equations, 
		 "[$vars]^( " 
		 . join(' and ',@eq) 
		 . " at "
		 . quote($node)
		 . ")"
		 );
	}
	push(@all,"equations => [\t".join(",\n\t\t\t\t",@equations)."\n\t\t\t]\n") if (@equations);
	print ":-tag_anchor{\t",join(",\n\t\t",@all),"\n\t}.\n\n";
    }
}

sub TAG::Document::header {
    my $self = shift;
    $map = shift || {};
    $header = TAG::Header->new;
    $_->header($self) foreach (values %{$self->{'lexicon'}});
    if (defined $map->{fs_add}) {
      my @cat = split(/\s+/,$map->{fs_add});
      $header->{'cat'}{$_}={} foreach (@cat);
    }
    if (defined $map->{'feature_add'}) {
      foreach my $cat (keys %{$map->{feature_add}}) {
	my @features = split(/\s+/,$map->{feature_add}{$cat});
	foreach my $f (@features) {
	  my $v='yes';
	  if ($f =~ /^not\((\S+)\)/) {
	    $f = $1;
	    $v = 'no';
	  }
	  $header->{'cat'}{$cat}{$f}=$v;
	}
      }
    }
    if (defined $map->{'fset'}) {
      foreach my $fset (keys %{$map->{fset}}) {
	my @values = split(/\s+/,$map->{'fset'}{$fset});
	$header->{'f'}{$fset}{$_}='yes' foreach (@values);
	$header->{'finite_set'}{$fset}='yes';
      }
    }
    if (defined $map->{fs_open}) {
      $header->{fs_open}{$_} = 1 foreach (keys %{$map->{fs_open}});
    }
    if (defined $map->{fs_fill}) {
      $header->{fs_fill}{$_} = 1 foreach (keys %{$map->{fs_fill}});
    }

    foreach my $lemma (values %{$self->{'lemma'}}){
	$_->header($self) foreach (values %$lemma);
    }
    $_->header() foreach (values %{$self->{'trees'}}); 
    return $header;
}

sub TAG::Lexicon::header {
    my $self = shift;
    $_->header() foreach (@{$self->{'lemma'}});
}

sub TAG::LemmaRef::header {
    my $self = shift;
    my $fs = $self->{'feature'};
    if ($fs) {
	$header->{'currentcat'} = $self->{'cat'};
	$fs->header();
    }
}

sub TAG::Lemma::header {
    $_->header() foreach (@{ shift->{'anchors'} });
}

sub TAG::Anchor::header {
    my $self = shift;
    $header->{'currentfamilies'} = $self->{'family'};
    $_->header() foreach (@{ $self->{'equations'} });
    $_->header() foreach (@{ $self->{'coanchors'} });
}

sub TAG::Coanchor::header {
    my $self = shift;
    my $node = $self->{'node_id'};
    foreach my $family (@{$header->{'currentfamilies'}}) {
	$header->{'anchors'}{$family}{'coanchor'}{$node} = 'yes';
    }
}

sub TAG::Equation::header {
    my $self = shift;
    my $id = $self->{'node_id'};
    ($header->{'currentcat'}) = ($id =~ /^(\w+)_/); # assume node id are derived from cat Cat_*
    $header->{'currenteq'} = $self;
    $self->{'feature'}->header();
    undef $header->{'currenteq'};
    undef $header->{'currentcat'};
}

sub TAG::Feature::header {
    my $self = shift;
    my $fstype = (defined $self->{type}) ? $self->{type} : $header->{'currentcat'};
    $_->header($fstype) foreach (values %{ $self->{'f'} });
}

sub TAG::Comp::header {
    my $self = shift;
    my $fstype = shift;
    my $f = $self->{'name'};
    my $eq = $header->{'currenteq'};
    my $kind = (defined $map->{'fs_redirect'}{$f}) ? $map->{'fs_redirect'}{$f} : $f;
    $header->{'cat'}{$fstype}{$f} ||= 'yes';
    $header->{'currentf'} = $kind;
    $header->{'finite_set'}{$kind} = 'yes' if (@{ $self->{'value'}} > 1);
    my $saveeq = $eq;
    if ($eq) {
	my @family = @{$header->{'currentfamilies'}};
	$header->{'anchors'}{$_}{'equation'}{$eq->{'node_id'}}{$eq->{'type'}}{$f} = 'yes' foreach (@family);
	undef $header->{currenteq};
    }
    my $savecat =  $header->{currentcat};
    $header->{currentcat} = $kind;
    $_->header foreach (@{ $self->{'value'} });
    $header->{currentcat} = $savecat;
    $header->{currenteq} = $saveeq;
}

sub TAG::Narg::header {
    my $self = shift;
    my $fstype = shift;
    $_->header($fstype) foreach (@{ $self->{'value'} });
}

sub TAG::Or::header {
  my $self = shift;
  $_->header foreach (@{$self->{guards}});
}

sub TAG::And::header {
  my $self = shift;
  $_->header foreach (@{$self->{guards}});
}

sub TAG::NGuards::header {
  my $self = shift;
  $_->header foreach (@{$self->{guards}});
}

sub TAG::NGuard::header {
  my $self = shift;
  my $kind = $self->{type};
  $kind =  $map->{'fs_redirect'}{$kind} if ($kind && defined $map->{'fs_redirect'}{$kind});
  $header->{'currentf'} = $kind || "";
  my $savecat =  $header->{currentcat};
  $header->{currentcat} = $kind;
  $_->header foreach (@{ $self->{'values'} });
  $header->{currentcat} = $savecat;
}

sub TAG::NXGuard::header {
  my $self = shift;
  my $kind = $self->{type};
  $kind =  $map->{'fs_redirect'}{$kind} if (defined $map->{'fs_redirect'}{$kind});
  $header->{'currentf'} = $kind;
  my $savecat =  $header->{currentcat};
  $header->{currentcat} = $kind;
  $_->header foreach (@{ $self->{'values'} });
  $header->{currentcat} = $savecat;
}

sub TAG::Val::header {
    $header->{'f'}{$header->{'currentf'}}{shift->{'text'}} = 'yes';
}

sub TAG::Var::header {
  my $self = shift;
  $header->{'finite_set'}{$header->{'currentf'}} = 'yes' if (@{ $self->{'value'}} > 1);
  $_->header foreach (@{$self->{'value'}});
}

sub TAG::Not::header {
  my $self = shift;
  $header->{'finite_set'}{$header->{'currentf'}} = 'yes';
  $_->header foreach (@{$self->{'value'}});
}

sub TAG::VAlt::header {
  my $self = shift;
  $header->{'finite_set'}{$header->{'currentf'}} = 'yes';
  $_->header foreach (@{$self->{'value'}});
}

sub TAG::Plus::header {
    $header->{'f'}{$header->{'currentf'}}{'+'} = 'yes';
}

sub TAG::Minus::header {
    $header->{'f'}{$header->{'currentf'}}{'-'} = 'yes';
}

sub TAG::Tree::header {
  my $self=shift;
  $self->{'root'}->header();
  if (defined $self->{ht}) {
    $header->{'currentcat'} = 'ht';
    $self->{'ht'}->header();
    undef $header->{'currentcat'};
  }
}

sub TAG::Optional::header {
  my $self = shift;
  $self->{node}->header();
}

sub TAG::Interleave::header {
  my $self = shift;
  $_->header() foreach (@{$self->{child}});
}

sub TAG::Sequence::header {
  my $self = shift;
  $_->header() foreach (@{$self->{child}});
  $_->header() foreach (@{ $self->{'guards'} });
}

sub TAG::Alternative::header {
  my $self = shift;
  $_->header() foreach (@{$self->{child}});
  $_->header() foreach (@{ $self->{'guards'} });
}

sub TAG::Node::header {
    my $self = shift;
    my $cat = $self->{'cat'};
    my $top = $self->{'top'};
    my $bot = $self->{'bot'};
    my $id = $self->{'id'};
    $header->{'currentcat'} = $cat if ($cat);
    $top->header($cat) if ($top);
    $bot->header($cat) if ($bot);
    $_->header() foreach (@{ $self->{'child'} });
    $_->header() foreach (@{ $self->{'guards'} });
    my $lex = $self->{lex};
    if (defined $lex) {
      $lex =~ s/^\^\s+(\S+)\s+\^\s*//o;
      ##      my @lex = split(/\s+/,$lex);
      my @lex = ();
      while ($lex =~ /\G\s*('.+?'|\S+)/og) {
	my $u = $1;
	$u =~ /^'(.+)'$/ and $u = $1;
	push(@lex,$u);
      }
      if (scalar(@lex) > 1) {
	my $lexset = $map->{lp}{lex_set} ||= 'lex';
	$header->{finite_set}{$lexset} = 'yes';
	foreach my $v (@lex) {
	  $header->{f}{$lexset}{$v} = 'yes';
	}
      }
    }

    if (defined $id) {
      $id =~ s/^\^\s+(\S+)\s+\^\s*//o;
      my @ids = split(/\s+/,$id);
      if (scalar(@ids) > 1) {
	my $nodeid = $map->{lp}{nodeid_set} ||= 'nodeid';
	$header->{finite_set}{$nodeid} = 'yes';
	foreach my $v (@ids) {
	  $header->{f}{$nodeid}{$v} = 'yes';
	}
      }
    }

  }

1;

__END__

=head1 NAME

TAG::Header

=head1 SYNOPSIS

 use TAG::Header;

=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

TAG (3)

=cut
