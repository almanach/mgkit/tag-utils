# $Id$
package TAG;

use 5.005;
use strict;
use warnings;
use Data::Grove;

our @ISA         = qw(Data::Grove);
our $VERSION = 1.13;
our $AUTOLOAD;

# nothing to be exported

sub AUTOLOAD {
    my $self = shift;

    my $name = $AUTOLOAD;
    $name =~ s/.*:://;
    return if $name eq 'DESTROY';

    if (@_) {
	return $self->{$name} = shift;
    } else {
	return $self->{$name};
    }
}

######################################################################
# TAG::Document

# Attributes: 
#     filename    String
#     Trees       Tree List
#     Lexicon     Lexicon List
#     Lemma       Lemma List
#     Families    Family List
#     Axiom       String

package TAG::Document;
our @ISA = qw{TAG};
our $type_name = 'document';


sub update {
    my $self = shift;
    my $updater = shift;
}

######################################################################
# TAG::Desc

# Attributes:
#       Test     String

package TAG::Desc;
our @ISA = qw{TAG};
our $type_name = 'desc';

######################################################################
# TAG::Family

# Attributes:
#       Name      String
#       Trees     arrayref of string

package TAG::Family;
our @ISA = qw{TAG};
our $type_name = 'family';

######################################################################
# TAG::Tree

# Attributes:
#       Name     String
#       Family   List of String
#       Type     {initial,auxiliary}
#       Root     Node
#       HyperTag Feature or empty

package TAG::Tree;
our @ISA = qw{TAG};
our $type_name = 'tree';

######################################################################
# TAG::Node

# Attributes:
#       id      String
#       top     Feature or empty
#       bot     Feature or empty
#       adj     {yes/no/strict}
#       type    {subs/std/lex/anchor/foot}
#       child   Node Arrayref
#       pguard  NGuard
#       nguard  NGuard

package TAG::Node;
our @ISA = qw{TAG};
our $type_name = 'node';


######################################################################
# TAG::Optional (node)

# Attributes: 
#         node  TAG::Node
#         n     0,*,+ number of occurrences
#       pguard  NGuard
#       nguard  NGuard

package TAG::Optional;
our @ISA = qw{TAG};
our $type_name = 'optional';

######################################################################
# TAG::Interleave (node)

# Attributes: 
#         child  (nodes)
#       pguard  NGuard
#       nguard  NGuard

package TAG::Interleave;
our @ISA = qw{TAG};
our $type_name = 'interleave';

######################################################################
# TAG::Sequence (node)

# Attributes: 
#         child  (nodes)
#       pguard  NGuard
#       nguard  NGuard

package TAG::Sequence;
our @ISA = qw{TAG};
our $type_name = 'sequence';


######################################################################
# TAG::Alternative (node)

# Attributes: 
#         child  (nodes)
#       pguard  NGuard
#       nguard  NGuard

package TAG::Alternative;
our @ISA = qw{TAG};
our $type_name = 'alternative';


######################################################################
# TAG::Lemma

# Attributes:
#       Name    String
#       Anchors Anchor arrayref

package TAG::Lemma;
our @ISA = qw{TAG};
our $type_name = 'lemma';

######################################################################
# TAG::Lexicon

# Attributes:
#       Lex     String
#       Lemma   LemmaRef arrayref

package TAG::Lexicon;
our @ISA = qw{TAG};
our $type_name = 'lexicon';

######################################################################
# TAG::LemmaRef

# Attributes:
#       Name      String
#       Cat       String
#       Feature   Feature

package TAG::LemmaRef;
our @ISA = qw{TAG};
our $type_name = 'lemmaref';

######################################################################
# TAG::Feature

# Attributes:
#     F         list of (key,val list)

package TAG::Feature;
our @ISA = qw{TAG};
our $type_name = 'feature';

######################################################################
# TAG::Equation

# Attributes:
#    Type       {top/bot}
#    Feature    Feature

package TAG::Equation;
our @ISA = qw{TAG};
our $type_name = 'equation';

######################################################################
# TAG::Anchor

# Attributes:
#    Family     String
#    Equations  Equation arrayref
#    Coanchor   Coanchor arrayref

package TAG::Anchor;
our @ISA = qw{TAG};
our $type_name = 'anchor';

######################################################################
# TAG::Coanchor

# Attributes:
#    Node_id    String
#    Lex        String arrayref

package TAG::Coanchor;
our @ISA = qw{TAG};
our $type_name = 'coanchor';

######################################################################
# TAG::Lex

# Attributes:
#    Text       String

package TAG::Lex;
our @ISA = qw{TAG};
our $type_name = 'lex';

######################################################################
# TAG::Comp

# Attributes:
#    name      string
#    value     arrayref of Val, Minus or Plus
#    rel       eq (default) | neq

package TAG::Comp;
our @ISA = qw{TAG};
our $type_name = 'comp';

######################################################################
# TAG::Var

# Attributes:
#    id        string
#    value       arrayref of Val, Minus or Plus

# should avoid id on Narg and Comp by materializing variables

package TAG::Var;
our @ISA = qw{TAG};
our $type_name = 'var';


######################################################################
# TAG::Not

# Attributes:
#    value       arrayref of Val, Minus or Plus

package TAG::Not;
our @ISA = qw{TAG};
our $type_name = 'not';

######################################################################
# TAG::VAlt

# Attributes:
#    value       arrayref of Val, Minus or Plus

package TAG::VAlt;
our @ISA = qw{TAG};
our $type_name = 'vAlt';

######################################################################
# TAG::Narg

# Attributes:
#    name      string in top/bot
#    val       arrayref of Val, Minus or Plus

package TAG::Narg;
our @ISA = qw{TAG};
our $type_name = 'narg';

######################################################################
# TAG::NGuards

# Attributes:
#    guards       arrayref of NGuard or Or

package TAG::NGuards;
our @ISA = qw{TAG};
our $type_name = 'NGuards';

######################################################################
# TAG::NGuard

# Attributes:
#    values       arrayref of Val, Minus or Plus

package TAG::NGuard;
our @ISA = qw{TAG};
our $type_name = 'NGuard';


######################################################################
# TAG::NXGuard

# Attributes:
#    values       arrayref of Val, Minus or Plus

package TAG::NXGuard;
our @ISA = qw{TAG};
our $type_name = 'NXGuard';

######################################################################
# TAG::Val

# Attributes:
#    Text       String

package TAG::Val;
our @ISA = qw{TAG};
our $type_name = 'val';

######################################################################
# TAG::Link

# Attributes:
#    Id       String

package TAG::Link;
our @ISA = qw{TAG};
our $type_name = 'link';

######################################################################
# TAG::Plus

package TAG::Plus;
our @ISA = qw{TAG};
our $type_name = 'plus';

######################################################################
# TAG::Minus

package TAG::Minus;
our @ISA = qw{TAG};
our $type_name = 'minus';

######################################################################
# TAG::Or

# Attributes:
#    guards       array NGuard or And

package TAG::Or;
our @ISA = qw{TAG};
our $type_name = 'or';

######################################################################
# TAG::And

# Attributes:
#    guards       array NGuard

package TAG::And;
our @ISA = qw{TAG};
our $type_name = 'and';

######################################################################
# TAG::Reducer

# Attributes:
#    value       array Val Var ...

package TAG::Reducer;
our @ISA = qw{TAG};
our $type_name = 'reducer';

1;

__END__

=head1 NAME

TAG - Perl-style objects to represent Tree Adjoining Grammars [TAG]

=head1 SYNOPSIS

 use TAG;
    
 # Basic parsing
 use TAG::XML::Parser;
 $parser = TAG::XML::Parser->new ();
 $document = $parser->parsefile( 'filename' );

 # Creating new objects
 $document = TAG::Document->new();
 $tree = TAG::Tree->new( name => 'w0tn1pn2', family => 'tn1pn2' );
 $node = TAG::Node->new( cat => 'NP', id => 'NP_0', type => 'subst' );

 # Accessing TAG objects
 $trees = $tree->{Trees};
 $tree = $tree->{Trees}{'tn1pn2'}{'w0tn1pn2'};
 $tree_name = $tree->{'name'};
  
=head1 DESCRIPTION

TAG uses Grove to implement a tree-based object model for representing
Tree Adjoining Grammars [TAG]. Following the XTAG description, such a
grammar provides elementary trees grouped into families, lemma
entries, and lexicon entries.

=head2 How To Create a Grove

There are several ways for TAGs to come into being, they can be read
from a file or string using a parser, or they can be created by your
Perl code using the `C<new()>' methods of TAG.

The most common way to build a TAG::Document object is using a parser,
for instance TAG::XML::Parser, which is an instance of XML::Parser.

=head2 Using TAGs

The properties provided by parsers are available directly using Perl's
normal syntax for accessing hashes and arrays.  For example, to get
the name of a tree:

  $tree_name = $tree->{'name'};

The following is the minimal set of objects and their properties that
you are likely to get from all parsers:

=head2 TAG::Document

=over 12

=item families

An hash containing the families TAG::Family indexed by
family names.

=item trees

An hash containing the elementary trees TAG::Tree indexed by
tree names.

=item lemma

An hash of hash containing the lemma TAG::Lemma indexed by name and
category.

=item lexicon

An hash containing the lexicon entries TAG::Lexicon indexed by the
lexical form.

=item desc

An arrayref of descriptions (TAG::Desc).

=back

=head2 TAG::Family

The Family objects represent families

=over 12

=item name

A string, the name of the family

=item trees

An array ref of tree names

=item desc

An arrayref of descriptions (TAG::Desc).

=back

=head2 TAG::Tree

The Tree objects represent trees.

=over 12

=item name

A string, the name of the tree

=item family

An array ref of strings, the name of the families the tree belongs to.

=item type

A string in 'initial' and 'auxiliary'

=item root

A TAG::Node, root node of the tree

=item desc

An arrayref of descriptions (TAG::Desc).

=back

=head2 TAG::Node

A node in a Tree object.

=over 12

=item id

A string, the id of the node

=item cat

A string, the syntactic category of the node

=item type

A string in 'std', 'subst', 'foot', 'lex', or 'anchor' denoting the
type of the node.

=item adj

A string in 'yes', 'no', or 'strict' denoting adjoining conditions on
the node.

=item lex

A string, the lexical value of the node of type 'lex'

=item top

Empty or a TAG::Feature object, representing the 'top' argument of
the node

=item bot

Empty or a TAG::Feature object, representing the 'bot' argument of the
node

=item child

An array of TAG::Node objects, the child of the node

=back

=head2 TAG::Lemma

A lemma entry:

=over 12

=item name

A string, the name of the lemma

=item cat

A string, the syntactic category of the lemma

=item anchors

An array of TAG::Anchor objects that indicate which trees may be
anchored by the lemma.

=item desc

An arrayref of descriptions (TAG::Desc).

=back


=head2 TAG::Lexicon

A lexicon entry:

=over 12

=item lex

A string, the lexical form of the entry

=item lemma

An arrayref of TAG::LemmaRef objects that indicate which lemma are
possible for this lexical entry.

=item desc

An arrayref of descriptions (TAG::Desc).

=back

=head2 TAG::LemmaRef

A reference to a lemma entry TAG::Lemma in a lexicon entry:

=over 12

=item name

A string, the name of the lemma

=item cat

A string, the syntactic category of the lemma

=item feature

A TAG::Feature object, denoting additional constraints on the lemma. 

=back

=head2 TAG::Anchor

A possible anchoring for a lemma entry (TAG::Lemma):

=over 12

=item family

An arrayref of strings, denoting the anchoring families

=item coanchors

An arrayref of TAG::Coanchor constraints

=item Equations

An arrayref of TAG::Equation constraints

=back

=head2 TAG::Coanchor

A constraint of co-anchoring in a TAG::Anchor object, stipulating that
some node should yield some lexical value.

=over 12

=item node_id

A string, denoting the node upon which the constraint holds

=item lex

An arrayref of TAG::Lex objects, denoting a disjunction of possible
lexical values for the node.

=back

=head2 TAG::Lex

A lexical value (in a TAG::Coanchor object):

=over 12

=item text

A string, denoting the lexical value

=back

=head2 TAG::Equation

An equation in a TAG::Anchor object that should hold on the 'top' or
'bot' argument of a node.

=over 12

=item node_id 

A string, denoting the node upon which the equation should hold.

=item type 

A string in 'top' or 'bot', denoting the argument of the node upon
which the equation should hold.

=item feature

A TAG::Feature object, representing the constraints

=back

=head2 TAG::Feature

A set of (feature,value), used in TAG::Equation, TAG::Node, and
TAG::LemmaRef objects:

=over 12

=item f 

A 'feature' indexed hashref of TAG:Comp objects

=back

=head2 TAG::Comp

To hold a value

=over 12

=item name

String, the name of the component

=item id

String (possibly empty), for multiple occurrences (variables).

=item value 

Array ref of values, represented by TAG::Val, TAG::Plus, or TAG::Minus
objects

=back

=head2 TAG::Val

A standard value in a TAG::Feature object:

=over 12

=item Text 

The value

=back

=head2 TAG::Link

** DEPRECATED ** see attribute id of TAG::Comp

=head2 TAG::Plus

A '+' value in a TAG::Feature object. No properties.

=head2 TAG::Minus

A '-' value in a TAG::Feature object. No properties.


=head2 TAG::Desc

A description option that can be attached to a document, a family, a
tree, a lemma, or a lexicon entry.

=over 12

=item text

A string, the content of the description

=back

=head1 METHODS

TAG by itself only provides one method, new(), for creating new TAG
objects.  There are Data::Grove extension modules that give additional
methods for working with TAG objects and new extensions can be created
as needed.

=over 4

=item $obj = TAG::OBJECT->new( [PROPERTIES] )

`C<new>' creates a new TAG object with the type I<OBJECT>, and with
the initial I<PROPERTIES>.  I<PROPERTIES> may be given as either a
list of key-value pairs, a hash, or a TAG object to copy.  I<OBJECT>
may be any of the objects listed above.

=back

=head1 WRITING EXTENSIONS

The class `C<TAG>' is the superclass of all classes in the XML::TAG
module.  `C<TAG>' is a subclass of `C<Data::Grove>'.

If you create an extension and you want to add a method to I<all> TAG
objects, then create that method in the TAG package. 

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie, Eric.De_La_Clergerie@inria.fr

=head1 SEE ALSO

Grove (3)
TAG::XML::Parser (3)
TAG::LP (3)
TAG::RCG (3)

Extensible Markup Language (XML) <http://www.w3c.org/XML>

=cut
